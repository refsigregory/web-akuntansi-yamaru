<?php
  require_once 'koneksi/database.php';

  $judul = "Berita | ".JUDUL;
  require_once 'template/Include/header.php';
?>

    <div id="site_content">
      <div id="sidebar_container">
        <div class="sidebar">
          <h3>Latest News</h3>
          <?php
            $query = $mysql->execute("select * from `berita` WHERE `status`='aktif' order by `tanggal` desc limit 5");
            
            while ($berita = $query->fetch_array())
            {
          ?>
          <h4><?php echo $berita['judul'];?></h4>
          <h5><?php echo $berita['tanggal'];?></h5>
          <p><?php echo limit_words($berita['berita'],10);?>...<a href="<?php echo URL_WEB;?>news.php?berita=<?php echo $berita['idBerita'];?>">Baca Selengkapnya</a></p>
          <?php
            }
          ?>
        </div>
      </div>
      <div class="content">
      <?php
            if(isset($_GET['berita'])){
              $id = $_GET['berita'];
              $queryn = $mysql->execute("select * from `berita` WHERE `idBerita`='".$mysql->clean($id)."' limit 1");
            }else {
              $queryn = $mysql->execute("select * from `berita` WHERE `status`='aktif' order by `tanggal` desc limit 5");
            }
          if($queryn->num_rows>0){ 
            while ($news = $queryn->fetch_array())
            {
      ?>

            <div style="margin-top: 10px; clear:both;">
              <h1 style="padding-bottom: 0;"><?php echo $news['judul']?></h1>
              <small><i style="padding: 5px 5px 5px 0px;">Dikirim pada <?php echo $news['tanggal'];?>. Oleh <b>Admin</b>.</i></small>
              <p>
              <?php if($news['gambar']!=NULL) {
                if(strlen($news['berita'])>250){
                echo '<img  width="75%" src="'.URL_WEB.'uploads/img/'.$news['gambar'].'" alt="'.$news['gambar'].'" style="float:left;padding: 0px 5px 5px 0px;" />';

                }else {
                  echo '<img  width="100%" height="80%" src="'.URL_WEB.'uploads/img/'.$news['gambar'].'" alt="'.$news['gambar'].'" style="padding:5px;" />';
                }
                  } ?>
                <?php echo $news['berita'];?>
              </p>
            </div>
      <?php
          
          }
          }else {
              echo "<p>Berita tidak ditemukan.</p>";
          }

      ?>
      </div>
    </div>
<?php
  require_once 'template/Include/footer.php';
?>
