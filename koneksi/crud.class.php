<?php
/********************************************************************************************************************************
*
* Class Koneksi By Refsi Sangkay
*
********************************************************************************************************************************/

class Crud {   

   function __construct()
   {
     include("kofigurasi.php");
     $this->db = new mysqli($hostname, $username, $password, $database);
     $this->error = $mysqlerror;
   }   

   function execute($query)
   {
     $result = $this->db->query($query);
     return $result;
   }

   function clean($data) {
      $filter_sql = $this->db->real_escape_string(stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
      return $filter_sql;
   }

   function error(){
      if($this->error){
        return $this->db->error;
      }else {
        return "";
      }
   }

}

/********************************************************************************************************************************
* Contoh penggunaan:
*
* $a = $crud->run("SELECT * FROM table");
********************************************************************************************************************************/