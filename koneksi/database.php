<?php
session_start();
require_once 'kofigurasi.php';

// memanggil class CRUD (koneksi)
include_once 'crud.class.php';
include_once 'function.php';

// inisasi class CRUD
$mysql = new Crud();

// variabel konstan
DEFINE('JUDUL', $judul);
DEFINE('URL_WEB', $url);

if(isset($_SESSION['project_r']['id'])){
// ammbil sesi
$sess = $_SESSION['project_r'];
// ambil data user yg login
$query = $mysql->execute("select * from `user` where idUser=$sess[id]");
$user = $query->fetch_array();
}

/**************************************************************************************************************
* Fungsi Tambahan
***************************************************************************************************************/

// Ambil nama akun
function nama_akun($id)
{
	global $mysql;

	$query = $mysql->execute("select * from `tbakun` where `kodeakun`='$id'");

	$akun = $query->fetch_array();

	return $akun['namaak'];
}

// Cek Jika Belum Login
function isLogin()
{
	if(!isset($_SESSION['project_r']['id'])){
		header("Location: ".URL_WEB."Login.html");
	}

}

// Cek Jika Sudah Login
function cekJikaLogin()
{
	if(isset($_SESSION['project_r']['level'])){
		header("Location: ".URL_WEB."aplikasi");
	}

}

// Halaman Khusus Admin
function isAdmin()
{
	if(($_SESSION['project_r']['level']!=1)){
		header("Location: ".URL_WEB."aplikasi");
	}

}

// Halaman Khusus Operator
function isOperator()
{
	if(($_SESSION['project_r']['level']!=2)){
		header("Location: ".URL_WEB."aplikasi");
	}

}

// Halaman Khusus User
function isUser()
{
	if(($_SESSION['project_r']['level']!=3)){
		header("Location: ".URL_WEB."aplikasi");
	}

}

// Halaman Khusus Admin & Operator
function isAdminOperator()
{
	if($_SESSION['project_r']['level']!=1 && $_SESSION['project_r']['level']!=2){
		header("Location: ".URL_WEB."aplikasi");
	}
}