<?php
/*********************************************************************************************************************************************
*
*	Fungsi by Refsi Sangkay
*
*********************************************************************************************************************************************/

/* 
* Menampilkan level admin
* 1 = Admin
* 2 = Operator
* 3 = User
*/
function getlevel($level)
{
	if(($level==1))
		{
			return "Admin";
		} else if (($level==2)) {	
			return "Operator";
		} else {
			return "User";
		}
}

function limit_words($string, $word_limit)
{
    $words = explode(" ",$string);
    return implode(" ",array_splice($words,0,$word_limit));
}
