<?php
  require_once 'koneksi/database.php';
  cekJikaLogin();

  $judul = "Masuk | ".JUDUL;
  require_once 'template/Include/header.php';
?>
    <div id="site_content">
      <div id="sidebar_container">
        <div class="sidebar">
          <h3>Latest News</h3>
          <?php
            $query = $mysql->execute("select * from `berita` WHERE `status`='aktif' order by `tanggal` desc limit 5");
            
            while ($berita = $query->fetch_array())
            {
          ?>
          <h4><?php echo $berita['judul'];?></h4>
          <h5><?php echo $berita['tanggal'];?></h5>
          <p><?php echo limit_words($berita['berita'],10);?>...<a href="<?php echo URL_WEB;?>news.php?berita=<?php echo $berita['idBerita'];?>">Baca Selengkapnya</a></p>
          <?php
            }
          ?>
        </div>
      </div>
      <div class="content">
        <h1>Login Page</h1>
        <h4><?php if(isset($_GET['err'])){ echo $_GET['err']; } ?></h4>
        <form id="contact" action="login/aksi.php" method="post">
          <div class="form_settings">
            <p><span>User Name</span><input class="contact" type="text" name="user" value="" /></p>
            <p><span>Password</span><input class="contact" type="password" name="pass" value="" /></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" value="LOGIN" /></p>
          </div>
        </form>
      </div>
    </div>
<?php
  require_once 'template/Include/footer.php';
?>