-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 06 Feb 2017 pada 09.57
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_db`
--
CREATE DATABASE IF NOT EXISTS `project_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `project_db`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `idBerita` int(11) NOT NULL,
  `judul` varchar(30) NOT NULL,
  `berita` text NOT NULL,
  `tanggal` date NOT NULL,
  `idUser` int(11) NOT NULL,
  `status` set('aktif','nonaktif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`idBerita`, `judul`, `berita`, `tanggal`, `idUser`, `status`) VALUES
(1, 'Berita Pertama', 'Testing berita pertama', '2017-01-11', 1, 'aktif'),
(3, 'Berita tidak aktif', 'test berita tidak aktif', '2017-01-23', 0, 'nonaktif'),
(6, 'Website Dalam Pengembangan', 'Untuk saat ini website ini masih dalam tahap pengembangan, mohon maaf jika ada fituer yang belum berfungsi.', '2017-01-23', 0, 'aktif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbakun`
--

CREATE TABLE `tbakun` (
  `kodeakun` varchar(7) NOT NULL,
  `namaak` varchar(60) NOT NULL,
  `saldonorm` set('DEBET','KREDIT') NOT NULL,
  `kelompok` set('AKTIVA','PASIVA') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbakun`
--

INSERT INTO `tbakun` (`kodeakun`, `namaak`, `saldonorm`, `kelompok`) VALUES
('1', 'Aktiva Lancar', 'DEBET', 'AKTIVA'),
('1-0', 'Kas dan Bank', 'DEBET', 'AKTIVA'),
('1-01', 'Kas', 'DEBET', 'AKTIVA'),
('1-01-01', 'Kas Yayasan', 'DEBET', 'AKTIVA'),
('1-01-02', 'Kas Kecil Yayasan', 'DEBET', 'AKTIVA'),
('1-01-03', 'Kas Kecil Akademi Fisioterapi', 'DEBET', 'AKTIVA'),
('1-01-04', 'Kas Kecil Sayap Kasih', 'DEBET', 'AKTIVA'),
('1-01-05', 'Kas Kecil A', 'DEBET', 'AKTIVA'),
('1-01-06', 'Kas Kecil B', 'DEBET', 'AKTIVA'),
('1-02', 'Bank Tidak Terikat', 'DEBET', 'AKTIVA'),
('1-02-01', 'Tabungan No. Rek. A', 'DEBET', 'AKTIVA'),
('1-02-02', 'Tabungan No. Rek. B', 'DEBET', 'AKTIVA'),
('1-02-10', 'Giro No. A', 'DEBET', 'AKTIVA'),
('1-02-11', 'Giro No. B', 'DEBET', 'AKTIVA'),
('1-03', 'Bank Terikat Sementara', 'DEBET', 'AKTIVA'),
('1-03-01', 'Tabungan No. Rek. C', 'DEBET', 'AKTIVA'),
('1-03-02', 'Tabungan No. Rek. D', 'DEBET', 'AKTIVA'),
('1-03-10', 'Giro No. C', 'DEBET', 'AKTIVA'),
('1-03-11', 'Giro No. D', 'DEBET', 'AKTIVA'),
('1-04', 'Bank Dana Pembangunan', 'DEBET', 'AKTIVA'),
('1-04-01', 'Tabungan Pembangunan No. A', 'DEBET', 'AKTIVA'),
('1-04-02', 'Tabungan Pembangunan No. B', 'DEBET', 'AKTIVA'),
('1-04-10', 'Giro Pembangunan No.', 'DEBET', 'AKTIVA'),
('1-1', 'Deposito', 'DEBET', 'AKTIVA'),
('1-10', 'Deposito Tidak Terikat', 'DEBET', 'AKTIVA'),
('1-10-01', 'Deposito A', 'DEBET', 'AKTIVA'),
('1-10-02', 'Deposito B', 'DEBET', 'AKTIVA'),
('1-11', 'Deposito Terikat Sementara', 'DEBET', 'AKTIVA'),
('1-11-01', 'Deposito C', 'DEBET', 'AKTIVA'),
('1-11-02', 'Deposito D', 'DEBET', 'AKTIVA'),
('1-12', 'Deposito Dana Pembangunan', 'DEBET', 'AKTIVA'),
('1-12-01', 'Deposito E', 'DEBET', 'AKTIVA'),
('1-12-02', 'Deposito F', 'DEBET', 'AKTIVA'),
('1-2', 'Piutang', 'DEBET', 'AKTIVA'),
('1-20', 'Piutang Karyawan', 'DEBET', 'AKTIVA'),
('1-20-01', 'Piutang a.n. A', 'DEBET', 'AKTIVA'),
('1-20-02', 'Piutang a.n. B', 'DEBET', 'AKTIVA'),
('1-21', 'Piutang Lain-lain', 'DEBET', 'AKTIVA'),
('1-21-01', 'Piutang A', 'DEBET', 'AKTIVA'),
('1-21-02', 'Piutang B', 'DEBET', 'AKTIVA'),
('1-3', 'Biaya Dibayar di Muka dan Uang Muka', 'DEBET', 'AKTIVA'),
('1-30', 'Biaya Dibayar di Muka', 'DEBET', 'AKTIVA');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbjurnal`
--

CREATE TABLE `tbjurnal` (
  `no_jurnal` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `total_debit` decimal(22,2) NOT NULL,
  `total_kredit` decimal(22,2) NOT NULL,
  `entry_user` varchar(20) NOT NULL,
  `waktu_entry` date NOT NULL,
  `disetujui_oleh` varchar(50) NOT NULL,
  `terima_bayar` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbjurnal`
--

INSERT INTO `tbjurnal` (`no_jurnal`, `tanggal`, `keterangan`, `total_debit`, `total_kredit`, `entry_user`, `waktu_entry`, `disetujui_oleh`, `terima_bayar`) VALUES
('1', '2017-02-02', 'Testing', '100000.00', '50000000000000.00', '1', '2017-02-02', 'Refsi Sangkay', 'tes'),
('2', '2017-02-02', 'Testing', '100000063887878.00', '5080000000000.00', '1', '2017-02-02', 'Refsi Sangkay', 'tes');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbjurnaldetil`
--

CREATE TABLE `tbjurnaldetil` (
  `no_id` int(11) NOT NULL,
  `no_jurnal` varchar(10) NOT NULL,
  `kodeakun` varchar(7) NOT NULL,
  `no_urut` int(11) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `jumlah_debet` decimal(22,2) NOT NULL,
  `jumlah_kredit` decimal(22,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbjurnaldetil`
--

INSERT INTO `tbjurnaldetil` (`no_id`, `no_jurnal`, `kodeakun`, `no_urut`, `keterangan`, `jumlah_debet`, `jumlah_kredit`) VALUES
(15, '1', '1-0', 1, 'test', '100000.00', '0.00'),
(16, '1', '1-01-01', 2, 'Cuma Test', '0.00', '50000000000000.00'),
(17, '2', '1-01', 1, 'Testing', '5000000.00', '0.00'),
(18, '2', '1-01-01', 2, 'Testing', '0.00', '5000000000000.00'),
(19, '2', '1', 3, 'test', '58887878.00', '0.00'),
(20, '2', '1-0', 4, '', '0.00', '80000000000.00'),
(21, '2', '1-01-02', 5, 'tsdfs', '100000000000000.00', '0.00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbjurnaldetiltemp`
--

CREATE TABLE `tbjurnaldetiltemp` (
  `no_id` int(11) NOT NULL,
  `kodeakun` varchar(7) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `jumlah_debet` decimal(22,2) NOT NULL,
  `jumlah_kredit` decimal(22,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbjurnaltemp`
--

CREATE TABLE `tbjurnaltemp` (
  `no_jurnal` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `total_debit` decimal(22,2) NOT NULL,
  `total_kredit` decimal(22,2) NOT NULL,
  `entry_user` varchar(20) NOT NULL,
  `waktu_entry` date NOT NULL,
  `disetujui_oleh` varchar(50) NOT NULL,
  `terima_bayar` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `idUser` int(11) NOT NULL,
  `user` varchar(20) NOT NULL,
  `pass` varchar(20) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`idUser`, `user`, `pass`, `level`) VALUES
(1, 'admin', 'admin', 1),
(2, 'operator', 'operator', 3),
(3, 'user', 'user', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`idBerita`),
  ADD KEY `idUser` (`idUser`);

--
-- Indexes for table `tbakun`
--
ALTER TABLE `tbakun`
  ADD PRIMARY KEY (`kodeakun`);

--
-- Indexes for table `tbjurnal`
--
ALTER TABLE `tbjurnal`
  ADD PRIMARY KEY (`no_jurnal`);

--
-- Indexes for table `tbjurnaldetil`
--
ALTER TABLE `tbjurnaldetil`
  ADD PRIMARY KEY (`no_id`),
  ADD KEY `kodeakun` (`kodeakun`),
  ADD KEY `no_jurnal` (`no_jurnal`);

--
-- Indexes for table `tbjurnaldetiltemp`
--
ALTER TABLE `tbjurnaldetiltemp`
  ADD PRIMARY KEY (`no_id`),
  ADD KEY `kodeakun` (`kodeakun`);

--
-- Indexes for table `tbjurnaltemp`
--
ALTER TABLE `tbjurnaltemp`
  ADD PRIMARY KEY (`no_jurnal`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`idUser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `idBerita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbjurnaldetil`
--
ALTER TABLE `tbjurnaldetil`
  MODIFY `no_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `tbjurnaldetiltemp`
--
ALTER TABLE `tbjurnaldetiltemp`
  MODIFY `no_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
