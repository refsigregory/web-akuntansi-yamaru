/**************************************************************************************************************
*
* JS By Refsi Sangkay
*
***************************************************************************************************************/
// Konfirmasi menghapus

function hapus(refsi){
    var tanya = confirm("Anda yakin menghapus Data "+ refsi +" ini?");
    if(tanya==true){
        return true;
    }else{
        return false;
    }
}

// Datepicker
  $(function() {
    $("#datepicker").datepicker({
      dateFormat: "yy-mm-dd"
    });
  });

// Inline Editor
$("#inline_editdetildebet").dblclick(function() {
    var value = $("#inline_editdetildebet").text();
    var id = $("#inline_editdetildebet").attr('data-id');
    $("#inline_editdetildebet").html("<form method='post' action=''><input type='hidden' name='aksi' value='editdebet'><input type='hidden' name='no_id' value='"+id+"'><input type='text' name='debet' value='"+value+"'>");
});

$("#inline_editdetilkredit").dblclick(function() {
    var value = $("#inline_editdetilkredit").text();
    var id = $("#inline_editdetildebet").attr('data-id');
    $("#inline_editdetilkredit").html("<form method='post' action=''><input type='hidden' name='aksi' value='editkredit'><input type='hidden' name='no_id' value='"+id+"'><input type='text' name='kredit' value='"+value+"'>");
});

// View Jurnal Detail
$(".view_detail").click(function() {
    var id = $(this).attr('data-id');

    $.ajax({  
                url:"list_detail.php?no_jurnal="+id+"",  
                type:"get",  
                success:function(data){
                     $("#jurnal_detail").fadeIn();
                     $('#list_detail').html(data);  
                }  
            });  
});


// View Jurnal Detail (Buku Besar)
$(".view_detail_bkbsr").click(function() {
    var id = $(this).attr('data-id');

    $.ajax({  
                url:"list_detail.php?no_jurnal="+id+"",  
                type:"get",  
                success:function(data){
                     $("#jurnal_detail").fadeIn();
                     $('#list_detail_bkbsr').html(data);  
                }  
            });  
});
// AJAX: Tambah Detail
function tambah_detail()  
      {
            var kodeakun    = $("#kodeakun").val();
            var keterangan  = $("#keterangan").val();
            var debet       = $("#debet").val();
            var kredit      = $("#kredit").val();

            $.ajax({  
                url:"aksi_detail.php?aksi=tambah&kodeakun="+kodeakun+"&keterangan="+keterangan+"&debet="+debet+"&kredit="+kredit+"",  
                type:"get",  
                success:function(data){
                    $("#tambah_detail").modal('toggle');
                     $('#detail_list').html(data);
                        $("#kodeakun").val("");
                        $("#keterangan").val("");
                        $("#debet").val("0");     
                        $("#kredit").val("0");     
                }  
            });  
      }


// AJAX: Tambah Detail
function hapus_detail()  
      {

            $.ajax({  
                url:"aksi_detail.php?aksi=hapusdetil",  
                type:"get",  
                success:function(data){
                     $('#detail_list').html(data);
                        $("#kodeakun").val("");
                        $("#keterangan").val("");
                        $("#debet").val("0");     
                        $("#kredit").val("0");     
                }  
            });  
      } 


// Datatables serverside: Daftar User
$(document).ready(function() {
        var dataTable = $('#lookup_user').DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax":{
                url :"../includes/datatables/daftar_user.php", // json datasource
                type: "post",  // method  , by default get
                error: function(){  // error handling
                    $(".lookup_user-error").html("");
                    $("#lookup_user").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#lookup_processing").css("display","none");
                    _user
                }
            }
        } );
    } );

// Datatables serverside: Berita
$(document).ready(function() {
        var dataTable = $('#lookup_berita').DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax":{
                url :"../includes/datatables/daftar_berita.php", // json datasource
                type: "post",  // method  , by default get
                error: function(){  // error handling
                    $(".lookup_berita-error").html("");
                    $("#lookup_berita").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#lookup_berita_processing").css("display","none");
                    
                }
            }
        } );
    } );

// Datatables serverside: Akun
$(document).ready(function() {
        var dataTable = $('#lookup_akun').DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax":{
                url :"../includes/datatables/daftar_akun.php", // json datasource
                type: "post",  // method  , by default get
                error: function(){  // error handling
                    $(".lookup_akun-error").html("");
                    $("#lookup_akun").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#lookup_akun_processing").css("display","none");
                    
                }
            }
        } );
    } );