<?php
error_reporting(0);
include_once '../../koneksi/database.php';
$myerror= $mysql->error();
isLogin();
isAdminOperator();

if(isset($_GET['hapus'])){
	if($_GET['hapus']){
		$query = $mysql->execute("delete from `tbjurnal` where `no_jurnal`='".$mysql->clean($_GET['hapus'])."'");
		if($query){
			$query = $mysql->execute("delete from `tbjurnaldetil` where `no_jurnal`='".$mysql->clean($_GET['hapus'])."'");
			$getmsg = "Sukses";
		}else {
			$geterror = "Gagal";
		}
	}
}
if(isset($_GET['verifikasi'])){
	if($_GET['verifikasi']){

		$query = $mysql->execute("update `tbjurnal` set `disetujui_oleh`='".$mysql->clean($user['user'])."' where `no_jurnal`='".$mysql->clean($_GET['verifikasi'])."'");
		
		if($query){
			 $detilquery = $mysql->execute("select * from `tbjurnaldetil` WHERE `no_jurnal`='".$_GET['verifikasi']."'");

			 $jurnalquery = $mysql->execute("select * from `tbjurnal` WHERE `no_jurnal`='".$_GET['verifikasi']."'");

			 $jurnal = $jurnalquery->fetch_assoc();

			 
			    // query total debet
			    $t_debitquery = $mysql->execute("select sum(`jumlah_debet`) as total_debit from `tbjurnaldetil` WHERE `no_jurnal`='".$_GET['verifikasi']."'");
			    //query total kredit
			    $t_kreditquery = $mysql->execute("select sum(`jumlah_kredit`) as total_kredit from `tbjurnaldetil` WHERE `no_jurnal`='".$_GET['verifikasi']."'");


			    $total_debit = $t_debitquery->fetch_row()[0];
			    $total_kredit = $t_kreditquery->fetch_row()[0];

			if($detilquery->num_rows>0){

				// insert detil yang diambil darti table tbjurmaldetiptemp
	      		while ($detil = $detilquery->fetch_array()){
	      		$queryakun = $mysql->execute("select * from `tbakun` where `kodeakun`='".$detil['kodeakun']."'");

	         	$akun = $queryakun->fetch_assoc();

	         	$mutasidetilquery = $mysql->execute("select * from `mutasi_akun_detail` WHERE `kodeakun`='".$detil['kodeakun']."' ORDER BY `no_index` DESC LIMIT 1");
	         	if($mutasidetilquery->num_rows>0){
			 		$mutasidetil = $mutasidetilquery->fetch_assoc();
			 	}else {
			 		$mutasidetil = array(
			 							'debet' 	=> 0,
			 							'kredit' 	=> 0,
			 							'saldo' 		=> 0
			 						);
			 	}
			 	$tanggal = $jurnal['tanggal'];
	         	$keterangan = $detil['keterangan'];
	         	$debet = $detil['jumlah_debet'];
	         	$kredit = $detil['jumlah_kredit'];

	         	if($akun['saldonorm']=='DEBET'){
		         	$saldo_debet = $mutasidetil['saldo_debet']+$debet-$kredit;
		         	
		         	$saldo_kredit = 0;

	         		$saldo = $mutasidetil['saldo']+($debet-$kredit);
	         	}else {

	         		// kredit
	         		$saldo_debet = 0;
		         	
		         	$saldo_kredit = $mutasidetil['saldo_kredit']+$kredit-$debet;
		         	$saldo = $mutasidetil['saldo']+($kredit-$debet);
	         	}


	         	$updatemutasidetail = $mysql->execute("insert into `mutasi_akun_detail` (`kodeakun`,`tanggal`,`keterangan`,`debet`,`kredit`,`saldo_debet`,`saldo_kredit`,`saldo`,`no_jurnal`) values('".$detil['kodeakun']."','".$tanggal."','".$keterangan."','".$debet."','".$kredit."','".$saldo_debet."','".$saldo_kredit."','".$saldo."','".$detil['no_jurnal']."')");

	         	if($updatemutasidetail){
			         	$querymutasi = $mysql->execute("select * from `mutasi_akun` where `kodeakun`='".$detil['kodeakun']."'");

	         			$mutasi = $querymutasi->fetch_assoc();

			         	$saldoawal_debet = $mutasi['saldo_awal_debet'];
			         	$saldoawal_kredit = $mutasi['saldo_awal_kredit'];


			         	// update mutasi_akun
			      		$q_jumlah_mutasi = $mysql->execute("select sum(`debet`) as mutasi_debet, sum(`kredit`) as mutasi_kredit from `mutasi_akun_detail` WHERE `kodeakun`='".$detil['kodeakun']."'");
			      		$jumlah_mutasi = $q_jumlah_mutasi->fetch_assoc();

			         	$mutasi_debet = $jumlah_mutasi['mutasi_debet'];
			         	$mutasi_kredit = $jumlah_mutasi['mutasi_kredit'];

			         	if($akun['saldonorm']=='DEBET'){
				         	//  AKTIVA
				         	$saldo_akhir_debet = $mutasi_debet-$mutasi_kredit;
				         	$saldo_akhir_kredit = 0;

				         	$perubahan = $saldo_debet-$saldo_kredit;

			         		$saldo_akhir = $saldo;
			         	}else {
			         		//  PASSIVA
				         	$saldo_akhir_kredit = ($mutasi_debet-$mutasi_kredit)*(-1);
				         	$saldo_akhir_debet = 0;

				         	$perubahan = $saldo_kredit-$saldo_debet;

			         		$saldo_akhir = $saldo;
			         	}


			         	/*if($mutasidetilquery->num_rows>0){
					 		$updatemutasidetail = $mysql->execute("update `mutasi_akun_detail` set `tanggal`='".$tanggal."', `keterangan`='".$keterangan."', `debet`='".$debet."',`kredit`='".$kredit."',`saldo_debet`='".$saldo_debet."',`saldo_kredit`='".$saldo_kredit."',`saldo`='".$saldo."' where `no_jurnal`='".$detil['no_jurnal']."'");
					 		if($updatemutasidetail){
			         			Header("Location: ".URL_WEB."aplikasi/list_jurnal/?msg=Sukses. ".$mysql->error()."");
			         		}else {
			         			header("Location: ".URL_WEB."aplikasi/list_jurnal/?error=Gagal: Update Mutasi Detail. ".$mysql->error()."");
			         		}
					 	}else {	*/
			         		
			         		$insertmutasi = $mysql->execute("update `mutasi_akun` set `mutasi_debet`='".$mutasi_debet."', `mutasi_kredit`='".$mutasi_kredit."', `saldo_akhir_debet`='".$saldo_akhir_debet."', `saldo_akhir_kredit`='".$saldo_akhir_kredit."', `perubahan`='".$perubahan."', `saldo_akhir`='".$saldo_akhir."' where `kodeakun`='".$detil['kodeakun']."'");

			         		if($insertmutasi){
			         			Header("Location: ".URL_WEB."aplikasi/list_jurnal/?msg=Sukses. ".$mysql->error()."");
			         		}else {
			         			header("Location: ".URL_WEB."aplikasi/list_jurnal/?error=Gagal: Mutasi Detail. ".$mysql->error()."");
			         		}
					 	//}
			 		}else {
			 			header("Location: ".URL_WEB."aplikasi/list_jurnal/?error=Gagal: Update Mutasi. ".$mysql->error()."");;
			 		}

      			}
      		}
			
		}else {
			header("Location: ".URL_WEB."aplikasi/list_jurnal/?error=Gagal. ".$mysql->error()."");
		}
	}
	die($mysql->error);
}


$judul = "List Jurnal";
include_once '../../template/Admin/header.php';
?>

    <div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-book"></i> List Jurnal</h3> 
        </div>
        <div class="panel-body">
          <table id="" class="table table-bordered table-hover">  
          <form class="form-horizontal col-xs-6" method="post" action="">
			<div class="form-group col-xs-2">
				<select name="bulan" class="form-control">
					<option value=""> - Bulan - </option>
					<option <?php echo ($_POST['bulan']=='01'? 'selected="selected"' : '')?> value="01">Januari</option>
					<option <?php echo ($_POST['bulan']=='02'? 'selected="selected"' : '')?> value="02">Februari</option>
					<option <?php echo ($_POST['bulan']=='03'? 'selected="selected"' : '')?> value="03">Maret</option>
					<option <?php echo ($_POST['bulan']=='04'? 'selected="selected"' : '')?> value="04">April</option>
					<option <?php echo ($_POST['bulan']=='05'? 'selected="selected"' : '')?> value="05">Mei</option>
					<option <?php echo ($_POST['bulan']=='06'? 'selected="selected"' : '')?> value="06">Juni</option>
					<option <?php echo ($_POST['bulan']=='07'? 'selected="selected"' : '')?> value="07">Juli</option>
					<option <?php echo ($_POST['bulan']=='08'? 'selected="selected"' : '')?> value="08">Agustus</option>
					<option <?php echo ($_POST['bulan']=='09'? 'selected="selected"' : '')?> value="09">September</option>
					<option <?php echo ($_POST['bulan']=='10'? 'selected="selected"' : '')?> value="10">Oktober</option>
					<option <?php echo ($_POST['bulan']=='11'? 'selected="selected"' : '')?> value="11">November</option>
					<option <?php echo ($_POST['bulan']=='12'? 'selected="selected"' : '')?> value="12">Desember</option>
				</select>
			</div>
			<div name="tahun" class="form-group col-xs-2">
				<select name="tahun" class="form-control">
					<option value=""> - Tahun - </option>
					<?php
					for($tahun=date("Y")+3;$tahun>=2010;$tahun--){
					?>
						<option <?php echo ($_POST['tahun'] == $tahun ? 'selected="selected"' : '');?> value="<?php echo $tahun;?>"><?php echo $tahun;?></option>
					<?php
					}
					?>
				</select>
			</div>
			<div class="form-group col-xs-2">
				<button class="btn btn-primary">Filter</button> <a href="#list_detail" class="btn btn-default">Lihat Detil</a>
			</div>
          </form>
             <thead bgcolor="#eeeeee" align="center">
              <tr>

              <th>Jurnal</th>
               <th>Tanggal </th>
               <th>Keterangan </th>
               <th>Total Debet </th>
               <th>Total Kredit </th>
               <th>Bulan </th>
               <th>Tahun </th>
               <th class="text-center"> </th>

              </tr>
             </thead>
              <tbody>
				<?php
					if(isset($_POST['bulan']) || isset($_POST['tahun'])){

						if($_POST['bulan']!=NULL && $_POST['tahun']!=NULL){
							// Berdasarkan Bulan dan Tahun
							$query = $mysql->execute("select *, year(`tanggal`) as tahun from tbjurnal where year(`tanggal`)='".$mysql->clean($_POST['tahun'])."' and month(`tanggal`)='".$mysql->clean($_POST['bulan'])."' ORDER BY `no_jurnal` DESC");
								$t_querydebit = $mysql->execute("select sum(`total_debit`) as total_s_debit from tbjurnal where year(`tanggal`)='".$mysql->clean($_POST['tahun'])."' and month(`tanggal`)='".$mysql->clean($_POST['bulan'])."'");
								$t_querykredit = $mysql->execute("select sum(`total_kredit`) as total_s_kredit from tbjurnal where year(`tanggal`)='".$mysql->clean($_POST['tahun'])."' and month(`tanggal`)='".$mysql->clean($_POST['bulan'])."'");
						}else if($_POST['bulan']==NULL && $_POST['tahun']!=NULL){
							// Berdasarkan Tahun
							$query = $mysql->execute("select *, year(`tanggal`) as tahun from tbjurnal where year(`tanggal`)='".$_POST['tahun']."' ORDER BY `no_jurnal` DESC");
							$t_querydebit = $mysql->execute("select sum(`total_debit`) as total_s_debit from tbjurnal where year(`tanggal`)='".$_POST['tahun']."'");
							$t_querykredit = $mysql->execute("select sum(`total_kredit`) as total_s_kredit from tbjurnal where year(`tanggal`)='".$_POST['tahun']."'");
						}else if($_POST['bulan']!=NULL && $_POST['tahun']==NULL){
							// Berdasarkan Bulan
							$query = $mysql->execute("select *, year(`tanggal`) as tahun from tbjurnal where month(`tanggal`)='".$mysql->clean($_POST['bulan'])."' ORDER BY `no_jurnal` DESC");
							$t_querydebit = $mysql->execute("select sum(`total_debit`) as total_s_debit from tbjurnal where month(`tanggal`)='".$mysql->clean($_POST['bulan'])."'");
							$t_querykredit = $mysql->execute("select sum(`total_kredit`) as total_s_kredit from tbjurnal where month(`tanggal`)='".$mysql->clean($_POST['bulan'])."'");
						}else {
							// Tidak Keduanya
							$query = $mysql->execute("select *, year(`tanggal`) as tahun from tbjurnal ORDER BY `no_jurnal` DESC");
							$t_querydebit = $mysql->execute("select sum(`total_debit`) as total_s_debit from tbjurnal");
							$t_querykredit = $mysql->execute("select sum(`total_kredit`) as total_s_kredit from tbjurnal");
						}
					}else{
						$query = $mysql->execute("select *, year(`tanggal`) as tahun from tbjurnal ORDER BY `no_jurnal` DESC");
						$t_querydebit = $mysql->execute("select sum(`total_debit`) as total_s_debit from tbjurnal");
						$t_querykredit = $mysql->execute("select sum(`total_kredit`) as total_s_kredit from tbjurnal");
					}
					
					$total_kredit 	= $t_querykredit->fetch_row()[0];
					$total_debit 	= $t_querydebit->fetch_row()[0];

					while ($jurnal = $query->fetch_array()){
				?>
					<tr class="view_detail" data-id="<?php echo $jurnal['no_jurnal'];?>">
						<td><?php echo $jurnal['no_jurnal'];?></td>
						<td><?php echo date("d-M-Y", strtotime($jurnal['tanggal']));?></td>
						<td><?php echo $jurnal['keterangan'];?></td>
						<td><?php echo number_format($jurnal['total_debit'],2);?></td>
						<td><?php echo number_format($jurnal['total_kredit'],2);?></td>
						<td><?php echo date("M", strtotime($jurnal['tanggal']));?></td>
						<td><?php echo $jurnal['tahun'];?></td>
						<td>
						<?php
							if($user['level']==1){
								if($jurnal['disetujui_oleh']==NULL) {
						?>
						<a href="?verifikasi=<?php echo $jurnal['no_jurnal']; ?>" title="Verifikasi Jurnal" onclick="return verifikasi(\'Jurnal\');" class="btn btn-warning"><i class="fa fa-check"></i></a>
						<?php } } ?>

						<?php
							if(($user['level']==1) || ($user['level']==2)){
								if($jurnal['disetujui_oleh']==NULL) {
						?>
						<a href="?hapus=<?php echo $jurnal['no_jurnal']; ?>" title="Hapus Jurnal" onclick="return hapus(\'Jurnal\');" class="btn btn-danger"><i class="fa fa-trash"></i></a>
						<?php } } ?>

						</td>
					</tr>
				<?php
				}
				?>
				<tr>
					<th colspan="3">Jumlah Total</th><th><?php echo number_format($total_debit,2);?></th><th><?php echo number_format($total_kredit,2);?></th>
				</tr>
              </tbody>
          </table>
        </div>
    </div>

    <div class="panel panel-default" id="jurnal_detail">
        <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-book"></i> Detail</h3> 
        </div>
        <div class="panel-body">
          <table id="" class="table table-bordered table-hover">  
             <thead bgcolor="#eeeeee" align="center">
              <tr>

              <th>Jurnal</th>
               <th>No </th>
               <th>Kode Akun </th>
               <th>Kode </th>
               <th>Keterangan </th>
               <th>Debet </th>
               <th>Kredit </th>
              
              </tr>
             </thead>
              <tbody id="list_detail">
				
              </tbody>
          </table>
        </div>
    </div>
<?php
include_once '../../template/Admin/footer.php';
?>

