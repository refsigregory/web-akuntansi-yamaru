<?php
include_once '../../koneksi/database.php';
isLogin();
isAdminOperator();

$judul = "Buku Besar";
include_once '../../template/Admin/header.php';
?>

    <div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-book"></i> List Jurnal</h3> 
        </div>
        <div class="panel-body">
          <table id="" class="table table-bordered table-hover">  
          <form class="form-horizontal col-xs-6" method="post" action="">
			<div class="form-group col-xs-2">
				<select name="bulan" class="form-control">
					<option value=""> - Bulan - </option>
					<option <?php echo ($_POST['bulan']=='01'? 'selected="selected"' : '')?> value="01">Januari</option>
					<option <?php echo ($_POST['bulan']=='02'? 'selected="selected"' : '')?> value="02">Februari</option>
					<option <?php echo ($_POST['bulan']=='03'? 'selected="selected"' : '')?> value="03">Maret</option>
					<option <?php echo ($_POST['bulan']=='04'? 'selected="selected"' : '')?> value="04">April</option>
					<option <?php echo ($_POST['bulan']=='05'? 'selected="selected"' : '')?> value="05">Mei</option>
					<option <?php echo ($_POST['bulan']=='06'? 'selected="selected"' : '')?> value="06">Juni</option>
					<option <?php echo ($_POST['bulan']=='07'? 'selected="selected"' : '')?> value="07">Juli</option>
					<option <?php echo ($_POST['bulan']=='08'? 'selected="selected"' : '')?> value="08">Agustus</option>
					<option <?php echo ($_POST['bulan']=='09'? 'selected="selected"' : '')?> value="09">September</option>
					<option <?php echo ($_POST['bulan']=='10'? 'selected="selected"' : '')?> value="10">Oktober</option>
					<option <?php echo ($_POST['bulan']=='11'? 'selected="selected"' : '')?> value="11">November</option>
					<option <?php echo ($_POST['bulan']=='12'? 'selected="selected"' : '')?> value="12">Desember</option>
				</select>
			</div>
			<div name="tahun" class="form-group col-xs-2">
				<select name="tahun" class="form-control">
					<option value=""> - Tahun - </option>
					<?php
					for($tahun=date("Y")+3;$tahun>=2010;$tahun--){
					?>
						<option <?php echo ($_POST['tahun'] == $tahun ? 'selected="selected"' : '');?> value="<?php echo $tahun;?>"><?php echo $tahun;?></option>
					<?php
					}
					?>
				</select>
			</div>
			<div class="form-group col-xs-2">
				<button class="btn btn-primary">Filter</button> <a href="#list_detail_bkbsr" class="btn btn-default">Lihat Detil</a>
			</div>
          </form>
             <thead bgcolor="#eeeeee" align="center">
              <tr>

              <th>Kode</th>
               <th>Nama Akun </th>
               <th>Awal Debet</th>
               <th>Awal Kredit </th>
               <th>Mutasi Debet </th>
               <th>Mutasi Kredit </th>
               <th>Akhir Debet </th>
               <th>Akhir Kredit </th>
			   <th>Saldo Awal </th>
			   <th>Perubahan</th>
			   <th>Saldo Akhir</th>

              </tr>
             </thead>
              <tbody>
				<?php
					if(isset($_POST['bulan']) || isset($_POST['tahun'])){

						if($_POST['bulan']!=NULL && $_POST['tahun']!=NULL){
							// Berdasarkan Bulan dan Tahun
							$query = $mysql->execute("select *, year(`tanggal`) as tahun from tbjurnal where year(`tanggal`)='".$mysql->clean($_POST['tahun'])."' and month(`tanggal`)='".$mysql->clean($_POST['bulan'])."' ORDER BY `no_jurnal` DESC");
								$t_querydebit = $mysql->execute("select sum(`total_debit`) as total_s_debit from tbjurnal where year(`tanggal`)='".$mysql->clean($_POST['tahun'])."' and month(`tanggal`)='".$mysql->clean($_POST['bulan'])."'");
								$t_querykredit = $mysql->execute("select sum(`total_kredit`) as total_s_kredit from tbjurnal where year(`tanggal`)='".$mysql->clean($_POST['tahun'])."' and month(`tanggal`)='".$mysql->clean($_POST['bulan'])."'");
						}else if($_POST['bulan']==NULL && $_POST['tahun']!=NULL){
							// Berdasarkan Tahun
							$query = $mysql->execute("select *, year(`tanggal`) as tahun from tbjurnal where year(`tanggal`)='".$_POST['tahun']."' ORDER BY `no_jurnal` DESC");
							$t_querydebit = $mysql->execute("select sum(`total_debit`) as total_s_debit from tbjurnal where year(`tanggal`)='".$_POST['tahun']."'");
							$t_querykredit = $mysql->execute("select sum(`total_kredit`) as total_s_kredit from tbjurnal where year(`tanggal`)='".$_POST['tahun']."'");
						}else if($_POST['bulan']!=NULL && $_POST['tahun']==NULL){
							// Berdasarkan Bulan
							$query = $mysql->execute("select *, year(`tanggal`) as tahun from tbjurnal where month(`tanggal`)='".$mysql->clean($_POST['bulan'])."' ORDER BY `no_jurnal` DESC");
							$t_querydebit = $mysql->execute("select sum(`total_debit`) as total_s_debit from tbjurnal where month(`tanggal`)='".$mysql->clean($_POST['bulan'])."'");
							$t_querykredit = $mysql->execute("select sum(`total_kredit`) as total_s_kredit from tbjurnal where month(`tanggal`)='".$mysql->clean($_POST['bulan'])."'");
						}else {
							// Tidak Keduanya
							$query = $mysql->execute("select *, year(`tanggal`) as tahun from tbjurnal ORDER BY `no_jurnal` DESC");
							$t_querydebit = $mysql->execute("select sum(`total_debit`) as total_s_debit from tbjurnal");
							$t_querykredit = $mysql->execute("select sum(`total_kredit`) as total_s_kredit from tbjurnal");
						}
					}else{
						$query = $mysql->execute("select *, year(`tanggal`) as tahun from mutasi_akun ORDER BY `kodeakun` DESC");
						$t_querydebit = $mysql->execute("select sum(`saldo_akhir_debet`) as total_s_debit from mutasi_akun");
						$t_querykredit = $mysql->execute("select sum(`saldo_akhir_kredit`) as total_s_kredit from mutasi_akun");
					}
					
					$total_kredit 	= $t_querykredit->fetch_row()[0];
					$total_debit 	= $t_querydebit->fetch_row()[0];

					while ($jurnal = $query->fetch_array()){
				?>
					<tr class="view_detail_bkbsr" data-id="<?php echo $jurnal['kodeakun'];?>">
						<td><?php echo $jurnal['kodeakun'];?></td>
						<td><?php echo nama_akun($jurnal['kodeakun']);?></td>
						<td><?php echo number_format($jurnal['saldo_awal_debet'],2);?></td>
						<td><?php echo number_format($jurnal['saldo_awal_kredit'],2);?></td>
						<td><?php echo number_format($jurnal['mutasi_debet'],2);?></td>
						<td><?php echo number_format($jurnal['mutasi_kredit'],2);?></td>
						<td><?php echo number_format($jurnal['saldo_akhir_debet'],2);?></td>
						<td><?php echo number_format($jurnal['saldo_akhir_kredit'],2);?></td>
						<td><?php echo number_format($jurnal['saldo_awal'],2);?></td>
						<td><?php echo number_format($jurnal['perubahan'],2);?></td>
						<td><?php echo number_format($jurnal['saldo_akhir'],2);?></td>
					</tr>
				<?php
				}
				?>
				<tr>
					<th colspan="6">Jumlah Total</th><th><?php echo number_format($total_debit,2);?></th><th><?php echo number_format($total_kredit,2);?></th>
				</tr>
              </tbody>
          </table>
        </div>
    </div>

    <div class="panel panel-default" id="jurnal_detail">
        <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-book"></i> Detail</h3> 
        </div>
        <div class="panel-body">
          <table id="" class="table table-bordered table-hover">  
             <thead bgcolor="#eeeeee" align="center">
              <tr>
				<td>Tanggal </td>
					<td>Keterangan</td>
					<td>No Ref</td>
					<td>Debet</td>
					<td>Kredit</td>
					<td>Saldo Debet</td>
					<td>Saldo Kredit</td>
					<td>Saldo</td>
              
              </tr>
             </thead>
              <tbody id="list_detail_bkbsr">
				
              </tbody>
          </table>
        </div>
    </div>
<?php
include_once '../../template/Admin/footer.php';
?>
