<?php
include_once '../../koneksi/database.php';
isLogin();
isAdmin();

$judul = "Input Saldo";
include_once '../../template/Admin/header.php';
?>
<div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-book"></i> Imput Saldo</h3> 
        </div>
        <div class="panel-body">
          <form action="tambah_aksi.php" method="post">
          <div class="">
            <p><label>Akun</label>
            <select class="form-control" id="kodeakun" name="kodeakun">
                <?php
                  $query = $mysql->execute("SELECT * FROM `tbakun`");
                  while ($akun = $query->fetch_array()) {
                ?>  
                <option id="" value="<?php echo $akun['kodeakun'];?>"><?php echo $akun['namaak'];?></option>
                <?php } ?>
              </select>
            </p>
           
            <p><label>Tanggal</label>
               <input type="text" class="form-control" name="tanggal" id="datepicker" required="required">
            </p>
             <p><label>Saldo Awal Kredit</label><input type="text"class="form-control" name="saldo_awal_kredit" value="0" /></p>
             <p><label>Saldo Awal Debet</label><input type="text"class="form-control" name="saldo_awal_debet" value="0" /></p>
             <p><label>Saldo Akhir Kredit</label><input type="text"class="form-control" name="saldo_akhir_kredit" value="0" /></p>
             <p><label>Saldo Akhir Debet</label><input type="text"class="form-control" name="saldoawal_debet" value="0" /></p>
             <p><label>Saldo Awal</label><input type="text"class="form-control" name="saldo_awal" value="0" /></p>
             <p><label>Saldo Akhir</label><input type="text"class="form-control" name="saldo_akhir" value="0" /></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="btn btn-primary" type="submit" name="name" value="Input Saldo" /></p>
          </div>
          </form>
        </div>
    </div>

      </div>
    </div>
<?php
include_once '../../template/Admin/footer.php';
?>

