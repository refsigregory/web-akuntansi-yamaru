<?php
include_once '../../koneksi/database.php';
isLogin();
isAdmin();

if((isset($_POST['judul'])) && (isset($_POST['berita'])))
{
	$judul	= $_POST['judul'];
	$berita	= $_POST['berita'];
	$status	= $_POST['status'];

	if(!empty($_FILES["gambar"]["tmp_name"])){
		$name = $_FILES['gambar']['name'];
		list($txt, $ext) = explode(".", $name);
		$target_dir = $_SERVER['DOCUMENT_ROOT']."/uploads/img/";
		$target_file = $target_dir . basename($_FILES["gambar"]["name"]);
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	    $check = getimagesize($_FILES["gambar"]["tmp_name"]);

		   
		    if($check!=false) {
		    	$gambarna = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
	             $tmp = $_FILES['gambar']['tmp_name'];
	             $nama_gambar = $gambarna;
	             if(move_uploaded_file($tmp, $target_dir.$gambarna))
	             {  
	        		$query = $mysql->execute("insert into `berita` (`judul`,`berita`, `gambar`, `tanggal`, `idUser`, `status`) values('".$mysql->clean($judul)."','".$mysql->clean($berita)."', '$nama_gambar', '".date("Y-m-d")."' ,'".$user['idUser']."', '".$mysql->clean($status)."')");
	    		} else {
	        		$error = "Sorry, there was an error uploading your file.";
	    		}
		    } else {
		        $error = "File is not an image.";
		        $query = FALSE;
		    }
		}else {
			$query = $mysql->execute("insert into `berita` (`judul`,`berita`, `tanggal`, `idUser`, `status`) values('".$mysql->clean($judul)."','".$mysql->clean($berita)."', '".date("Y-m-d")."' ,'".$user['idUser']."', '".$mysql->clean($status)."')");
		}
	if($query){
		header("Location: ".URL_WEB."aplikasi/berita/?msg=Sukses");
	}else {
		if(isset($error)){
				header("Location: ".URL_WEB."aplikasi/berita/?msg=$error");
			}else {
		header("Location: ".URL_WEB."aplikasi/berita/?msg=Gagal");
		}
	}

}else {
	$error = "Input tidak boleh kosong";
}

if(isset($error)){
	header("Location: ".URL_WEB."aplikasi/berita/?msg=$error");
}
