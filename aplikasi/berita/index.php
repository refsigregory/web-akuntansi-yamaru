<?php
include_once '../../koneksi/database.php';
isLogin();
isAdmin();

$judul = "Daftar Berita";
include_once '../../template/Admin/header.php';
?>
    <div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-newspaper-o"></i> Daftar Berita <a href="tambah.php"><button class="btn btn-primary">Tambah</button></a></h3> 
        </div>
        <div class="panel-body">
          <table id="lookup_berita" class="table table-bordered table-hover">  
             <thead bgcolor="#eeeeee" align="center">
              <tr>

              <th>No</th>
               <th>Gambar</th>
               <th>Judul </th>
               <th>Berita </th>
               <th>Tanggal </th>
               <th>Status </th>
               <th class="text-center"> Action </th> 

              </tr>
             </thead>
              <tbody>
              </tbody>
          </table>
        </div>
    </div>
<?php
include_once '../../template/Admin/footer.php';
?>

