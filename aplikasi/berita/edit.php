<?php
include_once '../../koneksi/database.php';
isLogin();
isAdmin();

$id = $_GET['id'];
$query = $mysql->execute("select * from berita where `idBerita`='".$mysql->clean($id)."' limit 1");

if ($query->num_rows>0){
  $edit = $query->fetch_array();
}else {
  header("Location: ".URL_WEB."aplikasi/berita/?err=Id tidak ditemukan");
}

$judul = "Edit Berita";
include_once '../../template/Admin/header.php';
?>
<div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-newspaper-o"></i> Edit Berita</h3> 
        </div>
        <div class="panel-body">
        <form action="edit_aksi.php" method="post">
          <input type="hidden" name="id" value="<?php echo $edit['idBerita'];?>">
          <div class="form_settings">
            <p><span>Judul</span><input type="text" class="form-control" name="judul" value="<?php echo $edit['judul'];?>" /></p>
             <p><span>Berita</span><textarea rows="8" class="form-control" cols="50" name="berita"><?php echo $edit['berita'];?></textarea></p>
            <p><span>Status</span>
              <select class="form-control" id="id" name="status">
                <option <?php echo ($edit['status']=='aktif' ? 'selected="selected"' : '');?> value="aktif">aktif</option>
                <option <?php echo ($edit['status']=='nonaktif' ? 'selected="selected"' : '');?> value="nonaktif">nonaktif</option>
              </select>
            </p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="btn btn-primary" type="submit" name="name" value="Edit" /></p>
          </div>
        </form>
      </div>
</div>
<?php
include_once '../../template/Admin/footer.php';
?>

