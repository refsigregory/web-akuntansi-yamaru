<?php
include_once '../../koneksi/database.php';
isLogin();
isAdmin();

$judul = "Daftar Berita";
include_once '../../template/Admin/header.php';
?>
<div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-newspaper-o"></i> Edit Berita</h3> 
        </div>

        <div class="panel-body">
        <form action="tambah_aksi.php" method="post" id="tambah_berita" enctype="multipart/form-data">
          <div class="form_settings">
            <p><span>Judul</span><input type="text" class="form-control" name="judul" value="" /></p>
             <p><span>Berita</span><textarea rows="8" class="form-control" cols="50" name="berita"></textarea></p>
            <p><span>Gambar</span><input type="file" name="gambar" id="upload_gambar"></p>
            <p><span>Status</span>
              <select class="form-control" id="id" name="status">
                <option value="1">aktif</option>
                <option value="2">nonaktif</option>
              </select>
            </p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="btn btn-primary" type="submit" name="name" value="Tambah" /></p>
          </div>
        </form>

      </div>
</div>
<?php
include_once '../../template/Admin/footer.php';
?>

