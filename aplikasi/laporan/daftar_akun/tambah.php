<?php
include_once '../../koneksi/database.php';
isLogin();

$judul = "Daftar Akun";
include_once '../../template/Admin/header.php';
?>

    <div id="site_content">
      <div class="content">
        <h1>Tambah Akun</h1>
        <form action="tambah_aksi.php" method="post">
          <div class="form_settings">
            <p><span>Username</span><input type="text" name="nama" value="" /></p>
            <p><span>Password</span><input type="text" name="pass" value="" /></p>
            <p><span>Level</span>
              <select id="id" name="level">
                <option value="1">User</option>
                <option value="2">Operator</option>
                <option value="3">Admin</option>
              </select>
            </p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="name" value="Tambah" /></p>
          </div>
        </form>

      </div>
    </div>
<?php
include_once '../../template/Admin/footer.php';
?>

