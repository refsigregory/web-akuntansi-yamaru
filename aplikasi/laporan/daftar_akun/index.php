<?php
include_once '../../../koneksi/database.php';
isLogin();

$judul = "Laporan Daftar User";
include_once '../../../template/Admin/header.php';
?>

    <div id="site_content">
      <div class="content">
        <h1>Laporan Daftar User</h1>
        <a href="tambah.php"><button>Tambah</button></a>
        <table style="width:100%; border-spacing:0;">
          <tr><th>No</th><th>Nama</th><th></th><th>Aksi</th></tr>
          <?php
            $query = $mysql->execute("select * from `user`");
            $no = 1;
            while ($akun = $query->fetch_array())
            {
          ?>
          <tr><td><?php echo $no;?>.</td><td><?php echo $akun['user'];?></td><td><?php echo $akun['user'];?></td><td><a href="edit.php?id=<?php echo $akun['idUser'];?>">Edit</a> &#183; <a href="hapus.php?id=<?php echo $akun['idUser'];?>">Hapus</a></td></tr>
        <?php
          $no++; 
            }
        ?>
        </table>

      </div>
    </div>
<?php
include_once '../../../template/Admin/footer.php';
?>

