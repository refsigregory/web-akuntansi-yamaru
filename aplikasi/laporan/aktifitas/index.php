<?php
include_once '../../../koneksi/database.php';
isLogin();

$judul = "Laporan Aktifitas";
if(isset($_GET['print'])){
include 'output.php';
exit();
}
include_once '../../../template/Admin/header.php';
?>
    <div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-file"></i> Laporan Aktifitas</h3> 
        </div>
        <div class="panel-body">
        <div class="container">
			<form method="post" action="?print">
				<div class="form-horizontal col-xs-12">
					<div class="form-group col-xs-2">
						<select class="form-control">
							<option> - Bulan -</option>
						</select>
					</div>
					<div class="form-group col-xs-2">
							<select class="form-control">
								<option> - Tahun - </option>
							</select>
					</div>
					<div class="form-group col-xs-1">
							<button class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
					</div>
				</div>
			</form>
		</div>
        </div>
    </div>
<?php
include_once '../../../template/Admin/footer.php';
?>

