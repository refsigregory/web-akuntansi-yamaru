<?php
include_once '../../koneksi/database.php';
isLogin();
isAdminOperator();

// Proses Tambah Detail
if(isset($_POST['aksi'])){
  if(($_POST['aksi']=='simpan')){
    // periode aktif
//    if(){
    $detilquery = $mysql->execute("select * from `tbjurnaldetiltemp`");

    // query total debet
    $t_debitquery = $mysql->execute("select sum(`jumlah_debet`) as total_debit from `tbjurnaldetiltemp`");

    //query total kredit
    $t_kreditquery = $mysql->execute("select sum(`jumlah_kredit`) as total_kredit from `tbjurnaldetiltemp`");

    $total_debit = $t_debitquery->fetch_row()[0];
    $total_kredit = $t_kreditquery->fetch_row()[0];

      $no_jurnal          = $_POST['no_jurnal'];
      $tanggal            = $_POST['tanggal'];
      $keterangan         = $_POST['keterangan'];
      $total_debit        = $total_debit;
      $total_kredit       = $total_kredit;
      $entry_user         = $user['idUser'];
      $waktu_entry        = date("Y-m-d");
      $terima_bayar       = $_POST['terima_bayar'];
      $saldo              = $total_debit-$total_kredit;

      $query = $mysql->execute("insert into `tbjurnal` (`no_jurnal`, `tanggal`, `keterangan`, `total_debit`,`total_kredit`,`entry_user`,`waktu_entry`,`terima_bayar`) values ('".$mysql->clean($no_jurnal)."','".$mysql->clean($tanggal)."','".$mysql->clean($keterangan)."','".$mysql->clean($total_debit)."','".$mysql->clean($total_kredit)."','".$mysql->clean($entry_user)."','".$mysql->clean($waktu_entry)."','".$mysql->clean($terima_bayar)."')");
      

      if($query){

        // insert detil yang diambil darti table tbjurmaldetiptemp
      while ($detil = $detilquery->fetch_array()){
          $insertjurnaldetail = $mysql->execute("insert into `tbjurnaldetil` (`no_jurnal`,`kodeakun`,`no_urut`,`keterangan`,`jumlah_debet`,`jumlah_kredit`) values ('".$mysql->clean($no_jurnal)."','".$mysql->clean($detil['kodeakun'])."','".$mysql->clean($detil['no_id'])."','".$mysql->clean($detil['keterangan'])."','".$mysql->clean($detil['jumlah_debet'])."','".$mysql->clean($detil['jumlah_kredit'])."')");
          if($insertjurnaldetail){
                  $insertmutasiakun = TRUE;//$mysql->execute("insert into `mutasi_akun_detail` (`kodeakun`,`tanggal`,`keterangan`,`debet`,`kredit`, `saldo`, `no_jurnal`) values ('".$mysql->clean($detil['kodeakun'])."','".$mysql->clean($tanggal)."','".$mysql->clean($detil['keterangan'])."','".$mysql->clean($detil['jumlah_debet'])."','".$mysql->clean($detil['jumlah_kredit'])."','".$mysql->clean($saldo)."','".$mysql->clean($no_jurnal)."')");

                    if($insertmutasiakun){
                          $mysql->execute("truncate `tbjurnaltemp`");
                          $mysql->execute("truncate `tbjurnaldetiltemp`");

                          header("Location: ".URL_WEB."aplikasi/entry_jurnal/?msg=Sukses".$mysql->error()."");
                    }
          }
      }
      }else {
        header("Location: ".URL_WEB."aplikasi/entry_jurnal/?msg=Gagal&$query".$mysql->error()."");
      }
//  } tutup periode aktif

  if(($_POST['aksi']=='hapusdetil')){
      $query = $mysql->execute("truncate `tbjurnaldetiltemp`");
      if($query){
        header("Location: ".URL_WEB."aplikasi/entry_jurnal/?msg=Sukses");
      }else {
        header("Location: ".URL_WEB."aplikasi/entry_jurnal/?error=Gagal. ".$mysql->error()."");
      }
  }
}
}
