<?php
include_once '../../koneksi/database.php';
isLogin();
isAdminOperator();

// Otomatis Kosongkan Tabel Jurlan Temp dan Jurlan Detil Temp saat refresh/reload halaman
$mysql->execute("truncate `tbjurnaltemp`");
$mysql->execute("truncate `tbjurnaldetiltemp`");

// Proses Tambah Detail
if(isset($_POST['aksi'])){
  if(($_POST['aksi']=='tambah')){
      $kodeakun   = $_POST['kodeakun'];
      $keterangan = $_POST['keterangan'];
      $debet      = $_POST['debet'];
      $kredit     = $_POST['kredit'];

      $query = $mysql->execute("insert into `tbjurnaldetiltemp` (`kodeakun`, `keterangan`, `jumlah_debet`, `jumlah_kredit`) values ('".$mysql->clean($kodeakun)."','".$mysql->clean($keterangan)."','".$mysql->clean($debet)."','".$mysql->clean($kredit)."')");
      if($query){
        $getmsg = "Sukses";
      }else {
        $geterror = "Gagal";
      }
  }


  if(($_POST['aksi']=='editdebet')){
      $query = $mysql->execute("update `tbjurnaldetiltemp` set `jumlah_debet`='".$mysql->clean($_POST['debet'])."' where no_id='".$mysql->clean($_POST['no_id'])."'");
      if($query){
        header("Location: ".URL_WEB."aplikasi/entry_jurnal/?msg=Sukses");
      }else {
        header("Location: ".URL_WEB."aplikasi/entry_jurnal/?error=Gagal");
      }
  }

  if(($_POST['aksi']=='editkredit')){
      $query = $mysql->execute("update `tbjurnaldetiltemp` set `jumlah_kredit`='".$mysql->clean($_POST['kredit'])."' where no_id='".$mysql->clean($_POST['no_id'])."'");
      if($query){
        header("Location: ".URL_WEB."aplikasi/entry_jurnal/?msg=Sukses");
      }else {
        header("Location: ".URL_WEB."aplikasi/entry_jurnal/?error=Gagal");
      }
  }
}


$judul = "Entry Jurnal";
include_once '../../template/Admin/header.php';
?>
    <div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-book"></i> Entry Jurnal
        </div>
        <div class="panel-body">
        <div class="form-horizontal">
        <form action="aksi.php" method="post">
            <div class="form-group">
            <label class="col-sm-2 control-label">No Jurnal</label>
            <div class="col-sm-2">
            <input type="text" class="form-control" name="no_jurnal" id="no_jurnal" required="required">
            </div>
            </div>
            <div class="form-group">
            <label class="col-sm-2 control-label">Tanggal</label>
            <div class="col-sm-2">
            <input type="text" class="form-control" name="tanggal" id="datepicker" required="required">
            </div>
            </div>
            <div class="form-group">
            <label class="col-sm-2 control-label">Keterangan</label>
            <div class="col-sm-5">
            <input type="text" name="keterangan" class="form-control" required="required">
            </div>
            </div>
            <div class="form-group">
            <label class="col-sm-2 control-label">Terima/Bayar</label>
            <div class="col-sm-3">
            <input type="text" name="terima_bayar" class="form-control" required="required">
            </div>
            </div>
        </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
        <p>
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tambah_detail">
          Tambah Detail
        </button>

        <a onclick="return hapus_detail();" class="btn btn-danger">
          Kosongkan Detail
        </a>
        </p>
        <p>
          <table class="table table-bordered table-hover">  
            <thead bgcolor="#eeeeee" align="center">
              <tr>
               <th>No </th>
               <th>Akun </th>
               <th>Kode </th>
               <th>Keterangan </th>
               <th>Debet </th>
               <th>Kredit </th>
              </tr>
            </thead>
            <tbody id="detail_list">
            </tbody>
          </table>
          </p>
        </div>
        <div class="panel-footer"><div style="float:right"><button type="submit" name="aksi" value="simpan" class="btn btn-primary">Simpan</button></form> <button class="btn btn-default">Batal</button> </div>
        <div style="clear:both"></div>
        </div>
    </div>


<!-- Modal -->
<div class="modal fade" id="tambah_detail" tabindex="-1" role="dialog" aria-labelledby="TambahDetailLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="TambahDetailLabel">Tambah Detail</h4>
      </div>
      <div class="modal-body">
          <form action="" method="post">
            <div class="form-group">
              <label>Akun</label>
              <select class="form-control" id="kodeakun" name="kodeakun">
                <?php
                  $query = $mysql->execute("SELECT * FROM `tbakun` WHERE `kodeakun` like '_______'");
                  while ($akun = $query->fetch_array()) {
                ?>  
                <option id="" value="<?php echo $akun['kodeakun'];?>"><?php echo $akun['namaak'];?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label>Keterangan</label>
              <input type="text" class="form-control" name="keterangan" id="keterangan">
            </div>
            <div class="form-group">
              <label>Debet</label>
              <input type="text" class="form-control" name="debet" id="debet" value="0">
            </div>
            <div class="form-group">
              <label>Kredit</label>
              <input type="text" class="form-control" name="kredit" id="kredit" value="0">
            </div>
            <button type="button" onclick="return tambah_detail()" id="" name="aksi" value="tambah"  class="btn btn-default">Tambah</button>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
<?php
include_once '../../template/Admin/footer.php';
?>

