<?php
include_once '../../koneksi/database.php';
isLogin();
isAdminOperator();

?>
<tbody>
<?php
  $query = $mysql->execute("select * from `tbjurnaldetiltemp`");
  if($query->num_rows>0){
  while ($detil = $query->fetch_array()) {
?>  
  <tr>
    <td><?php echo $detil['no_id'];?></td>
    <td><?php ?></td>
    <td><?php echo $detil['kodeakun'];?></td>
    <td><?php echo $detil['keterangan'];?></td>
    <td id="inline_editdetildebet" data-id="<?php echo $detil['no_id'];?>"><?php echo number_format($detil['jumlah_debet']);?></td>
    <td id="inline_editdetilkredit" data-id="<?php echo $detil['no_id'];?>"><?php echo number_format($detil['jumlah_kredit']);?></td>
    <td><a class="btn btn-danger" href=""><i class="fa fa-trash"></i></a></td>
  </tr>

<?php
  }

  $tquery_debet = $mysql->execute("select sum(`total_debet`) as total_debet from `tbjurnaldetiltemp`");
  $tquery_kredit = $mysql->execute("select sum(`total_kredit`) as total_kredit from `tbjurnaldetiltemp`");

  $total_debet = $tquery_debet->fetch_row()[0];
  $total_kredit = $tquery_kredit->fetch_row()[0];

?>
<tr bgcolor="#eee">
  <th colspan="4">TOTAL</th>
  <th><?php echo $total_debet;?></th>
  <th><?php echo $total_kredit;?></th>
</tr>
<?php
}else {

?>
  <tr>
    <td colspan="6">Data kosong</td>
  </tr>
<?php
}
?>
</tbody>