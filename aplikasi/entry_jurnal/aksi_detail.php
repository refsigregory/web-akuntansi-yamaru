<?php
include_once '../../koneksi/database.php';
isLogin();
isAdminOperator();

// Proses Tambah Detail
if(isset($_GET['aksi'])){
  if(($_GET['aksi']=='tambah')){
      $kodeakun   = $_GET['kodeakun'];
      $keterangan = $_GET['keterangan'];
      $debet      = $_GET['debet'];
      $kredit     = $_GET['kredit'];

      $query = $mysql->execute("insert into `tbjurnaldetiltemp` (`kodeakun`, `keterangan`, `jumlah_debet`, `jumlah_kredit`) values ('".$mysql->clean($kodeakun)."','".$mysql->clean($keterangan)."','".$mysql->clean($debet)."','".$mysql->clean($kredit)."')");
    }

    if(($_GET['aksi']=='hapusdetil')){
      $query = $mysql->execute("truncate `tbjurnaldetiltemp`");
  }

    if(($_GET['aksi']=='editdebet')){
      $query = $mysql->execute("update `tbjurnaldetiltemp` set `jumlah_debet`='".$mysql->clean($_GET['debet'])."' where no_id='".$mysql->clean($_GET['no_id'])."'");
  }

  if(($_GET['aksi']=='editkredit')){
      $query = $mysql->execute("update `tbjurnaldetiltemp` set `jumlah_kredit`='".$mysql->clean($_GET['kredit'])."' where no_id='".$mysql->clean($_GET['no_id'])."'");
  }
      if($query){
//Tampil jika sukses
?>


<?php
  $query = $mysql->execute("select * from `tbjurnaldetiltemp`");
  if($query->num_rows>0){
  while ($detil = $query->fetch_array()) {
?>  
  <tr>
    <td><?php echo $detil['no_id'];?></td>
    <td><?php echo nama_akun($detil['kodeakun'])?></td>
    <td><?php echo $detil['kodeakun'];?></td>
    <td><?php echo $detil['keterangan'];?></td>
    <td id="inline_editdetildebet" data-id="<?php echo $detil['no_id'];?>"><?php echo number_format($detil['jumlah_debet'],2);?></td>
    <td id="inline_editdetilkredit" data-id="<?php echo $detil['no_id'];?>"><?php echo number_format($detil['jumlah_kredit'],2);?></td>
  </tr>

<?php
  }

  $tquery_debet = $mysql->execute("select sum(`jumlah_debet`) as total_debet from `tbjurnaldetiltemp`");
  $tquery_kredit = $mysql->execute("select sum(`jumlah_kredit`) as total_kredit from `tbjurnaldetiltemp`");

  $total_debet = $tquery_debet->fetch_row()[0];
  $total_kredit = $tquery_kredit->fetch_row()[0];

?>
<tr bgcolor="#eee">
  <th colspan="4">TOTAL</th>
  <th><?php echo number_format($total_debet,2);?></th>
  <th><?php echo number_format($total_kredit,2);?></th>
</tr>
<?php
}else {

?>
  <tr>
    <td colspan="6">Detail kosong</td>
  </tr>
<?php
}
?>

<?php
      }else {
        echo "Tambah Gagal";
      }
}
?>