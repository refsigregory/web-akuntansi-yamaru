<?php
include_once '../../koneksi/database.php';
isLogin();
isAdmin();

$judul = "Tambah Akun";
include_once '../../template/Admin/header.php';
?>
<div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-book"></i> Tambah Akun</h3> 
        </div>
        <div class="panel-body">
          <form action="tambah_aksi.php" method="post">
          <div class="">
            <p><label>Kode Akun</label><input type="text"class="form-control" name="kodeakun" value="" /></p>
            <p><label>Nama Akun</label><input type="text"class="form-control" name="namaak" value="" /></p>
            <p><label>Saldo</label>
              <select class="form-control" id="id" name="saldonorm">
                <option value="DEBET">DEBET</option>
                <option value="KREDIT">KREDIT</option>
              </select>
            </p>
            <p><label>Kelompok</label>
              <select class="form-control" id="id" name="kelompok">
                <option value="AKTIVA">AKTIVA</option>
                <option value="PASIVA">PASIVA</option>
              </select>
            </p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="btn btn-primary" type="submit" name="name" value="Tambah" /></p>
          </div>
          </form>
        </div>
    </div>

      </div>
    </div>
<?php
include_once '../../template/Admin/footer.php';
?>

