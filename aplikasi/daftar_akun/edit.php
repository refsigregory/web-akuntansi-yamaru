<?php
include_once '../../koneksi/database.php';
isLogin();
isAdmin();

$id	= $_GET['id'];
$query = $mysql->execute("select * from tbakun where `kodeakun`='".$mysql->clean($id)."' limit 1");

if ($query->num_rows>0){
	$edit = $query->fetch_array();
}else {
	header("Location: ".URL_WEB."aplikasi/daftar_akun/?err=Id tidak ditemukan");
}

$judul = "Edit Akun";
include_once '../../template/Admin/header.php';
?>
<div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-user"></i> Edit User</h3> 
        </div>
        <div class="panel-body">
          <form action="edit_aksi.php" method="post">
          <input type="hidden" name="id" value="<?php echo $edit['kodeakun'];?>">
          <div class="">
            <p><label>Kode Akun</label><input type="text" class="form-control" name="kodeakun" value="<?php echo $edit['kodeakun'];?>" /></p>
            <p><label>Nama Akun</label><input type="text" class="form-control" name="namaak" value="<?php echo $edit['namaak'];?>" /></p>
            <p><label>Saldo</label>
              <select class="form-control" id="id" name="saldonorm">
                <option <?php echo ($edit['saldonorm']=='DEBET' ? 'selected="selected"' : '');?> value="DEBET">DEBET</option>
                <option <?php echo ($edit['saldonorm']=='KREDIT' ? 'selected="selected"' : '');?> value="KREDIT">KREDIT</option>
              </select>
            </p>
            <p><label>Kelompok</label>
              <select class="form-control" id="id" name="kelompok">
                <option <?php echo ($edit['kelompok']=='AKTIVA' ? 'selected="selected"' : '');?> value="AKTIVA">AKTIVA</option>
                <option <?php echo ($edit['kelompok']=='PASIVA' ? 'selected="selected"' : '');?> value="PASIVA">PASIVA</option>
              </select>
            </p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="btn btn-primary" type="submit" name="name" value="Edit" /></p>
          </div>
          </form>
        </div>
    </div>
<?php
include_once '../../template/Admin/footer.php';
?>

