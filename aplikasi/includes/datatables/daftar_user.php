 <?php
error_reporting(0);
/* Database connection start */
include_once "../../../koneksi/database.php";

// storing  request (ie, get/post) global array to a variable  
$requestData= $_REQUEST;


$columns = array( 
// datatable column index  => database column name
    0 => 'idUser',
    1 => 'user', 
    2 => 'level', 
);

// getting total number records without any search
$sql = "SELECT idUser, user, level ";
$sql.=" FROM user";
//$query=mysqli_query($conn, $sql) or die("data_user.php: get InventoryItems");
$query = $mysql->execute($sql);
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.


if( !empty($requestData['search']['value']) ) {
    // if there is a search parameter
    $sql = "SELECT idUser, user, level ";
    $sql.=" FROM user";
    $sql.=" WHERE user LIKE '".$requestData['search']['value']."%' ";    // $requestData['search']['value'] contains search parameter
    //$sql.=" OR idUser LIKE '".$requestData['search']['value']."%' ";
    $query = $mysql->execute($sql);
    //$query=mysqli_query($conn, $sql) or die("data_user.php: get PO");
    $totalFiltered = $query->num_rows; // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query 

    $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
    $query=$mysql->execute($sql) or die("data_user.php: get PO"); // again run query with limit
    
} else {    

    $sql = "SELECT idUser, user, level ";
    $sql.=" FROM user";
    $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
    
    //$query=mysqli_query($conn, $sql) or die("data_user.php: get PO");
    
}

$data = array();
while( $row= $query->fetch_array() ) {  // preparing an array mysqli_fetch_array($query)
    $nestedData=array(); 

    $nestedData[] = $row["idUser"];
    $nestedData[] = $row["user"];
    $nestedData[] = getlevel($row["level"]);
    $nestedData[] = '<td><center><a href="edit.php?id='.$row['idUser'].'" class="btn btn-primary"><i class="fa fa-pencil"></i> Edit</a> <a href="hapus.php?id='.$row['idUser'].'" onclick="return hapus(\'User\');" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</a></td></center>';      
    
    $data[] = $nestedData;
    
}



$json_data = array(
            "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal"    => intval( $totalData ),  // total number of records
            "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data"            => $data   // total data array
            );

echo json_encode($json_data);  // send data as json format

?>