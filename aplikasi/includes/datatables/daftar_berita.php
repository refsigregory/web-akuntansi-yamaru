<?php
error_reporting(0);
/* Database connection start */
include_once "../../../koneksi/database.php";

// storing  request (ie, get/post) global array to a variable  
$requestData= $_REQUEST;


$columns = array( 
// datatable column index  => database column name
    0 => 'idBerita',
    1 => 'gambar',
    2 => 'judul', 
    3 => 'berita',
    4 => 'tanggal',
    5 => 'status'
);

// getting total number records without any search
$sql = "SELECT * ";
$sql.=" FROM berita ORDER BY `tanggal` DESC";
//$query=mysqli_query($conn, $sql) or die("data_berita.php: get InventoryItems");
$query = $mysql->execute($sql);
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.


if( !empty($requestData['search']['value']) ) {
    // if there is a search parameter
    $sql = "SELECT *";
    $sql.=" FROM berita";
    $sql.=" WHERE judul LIKE '%".$requestData['search']['value']."%' ";    // $requestData['search']['value'] contains search parameter
    $sql.=" OR berita LIKE '%".$requestData['search']['value']."%' ";
    $query = $mysql->execute($sql);
    //$query=mysqli_query($conn, $sql) or die("data_berita.php: get PO");
    $totalFiltered = $query->num_rows; // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query 

    $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
    $query=$mysql->execute($sql) or die("data_berita.php: get PO"); // again run query with limit
    
} else {    

    $sql = "SELECT * ";
    $sql.=" FROM berita";
    $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
    
    //$query=mysqli_query($conn, $sql) or die("data_berita.php: get PO");
    
}

$data = array();
$target_dir = realpath(dirname(__DIR__))."/uploads/img/";
$no = 1;
while( $row= $query->fetch_array() ) {  // preparing an array mysqli_fetch_array($query)
    $nestedData=array(); 

    $nestedData[] = $no;
    if($row['gambar']!=NULL || $row['gambar']!=''){
        $nestedData[] = '<td><img width="200px" height="" src="'.URL_WEB.'uploads/img/'.$row['gambar'].'" /></td>';
    }else {
        $nestedData[] = '<td>Tidak ada gambar</td>';
    }
    $nestedData[] = $row["judul"];
    $nestedData[] = $row["berita"];
    $nestedData[] = $row["tanggal"];
    $nestedData[] = $row["status"];
    $nestedData[] = '<td><center><a href="edit.php?id='.$row['idBerita'].'" class="btn btn-primary"><i class="fa fa-pencil"></i></a> <a href="hapus.php?id='.$row['idBerita'].'" onclick="return hapus(\'Berita\');" class="btn btn-danger"><i class="fa fa-trash"></i></a></td></center>';      
    
    $data[] = $nestedData;
    $no++;
    
}



$json_data = array(
            "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal"    => intval( $totalData ),  // total number of records
            "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data"            => $data   // total data array
            );

echo json_encode($json_data);  // send data as json format

?>