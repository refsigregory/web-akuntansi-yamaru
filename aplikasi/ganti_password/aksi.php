<?php
include_once '../../koneksi/database.php';
isLogin();

if((isset($_POST['pass'])) && (isset($_POST['newpass1'])) && (isset($_POST['newpass2'])))
{
	$id			= $user['idUser'];
	$pass		= $_POST['pass'];
	$newpass1	= $_POST['newpass1'];
	$newpass2	= $_POST['newpass2'];


	if($pass==$user['pass']){
		if($newpass1==$newpass2){
			$newpass	= $_POST['newpass1'];

			$query = $mysql->execute("update `user` set `pass`='".$mysql->clean($newpass)."' where `idUser`='".$mysql->clean($id)."'");
			if($query){
				header("Location: ".URL_WEB."aplikasi/ganti_password/?msg=Sukses");
			}else {
				header("Location: ".URL_WEB."aplikasi/ganti_password/?error=Gagal");
			}
		}else {
			header("Location: ".URL_WEB."aplikasi/ganti_password/?error=Password baru tidak sama");
		}

	}else {
		header("Location: ".URL_WEB."aplikasi/ganti_password/?error=Password lama salah");
	}

}else {
	$error = "Input tidak boleh kosong";
}

if(isset($error)){
	header("Location: ".URL_WEB."aplikasi/ganti_password/?error=$error");
}
