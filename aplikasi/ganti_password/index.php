<?php
include_once '../../koneksi/database.php';
isLogin();

$id	= $user['idUser'];
$query = $mysql->execute("select * from user where `idUser`='".$mysql->clean($id)."' limit 1");

if ($query->num_rows>0){
	$edit = $query->fetch_array();
}else {
	header("Location: ".URL_WEB."aplikasi/ganti_password/?err=Id tidak ditemukan");
}

$judul = "Edit Password";
include_once '../../template/Admin/header.php';
?>
<div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-key"></i> Edit Password</h3> 
        </div>
        <div class="panel-body">
          <form action="aksi.php" method="post">
          <input type="hidden" name="id" value="<?php echo $edit['kodeakun'];?>">
          <div class="">
            <p><label>Masukan Password Lama</label><input type="password" class="form-control" name="pass" value="" /></p>
            <p><label>Password Baru</label><input type="password" class="form-control" name="newpass1" value="" /></p>
            <p><label>Masukan Lagi Password Baru</label><input type="password" class="form-control" name="newpass2" value="" /></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="btn btn-primary" type="submit" name="name" value="Edit" /></p>
          </div>
          </form>
        </div>
    </div>
<?php
include_once '../../template/Admin/footer.php';
?>

