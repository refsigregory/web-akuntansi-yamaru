<?php
include_once '../../koneksi/database.php';
isLogin();
isAdmin();

$judul = "Tambah Akun";
include_once '../../template/Admin/header.php';
?>
<div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-user"></i> Tambah User</h3> 
        </div>
        <div class="panel-body">
          <form action="tambah_aksi.php" method="post">
          <input type="hidden" name="id" value="<?php echo $edit['idUser'];?>">
          <div class="">
            <p><label>Username</label><input type="text"class="form-control" name="nama" value="" /></p>
            <p><label>Password</label><input type="text"class="form-control" name="pass" value="" /></p>
            <p><label>Level</label>
              <select class="form-control" id="id" name="level">
                <option value="3">User</option>
                <option value="2">Operator</option>
                <option value="1">Admin</option>
              </select>
            </p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="btn btn-primary" type="submit" name="name" value="Tambah" /></p>
          </div>
          </form>
        </div>
    </div>

      </div>
    </div>
<?php
include_once '../../template/Admin/footer.php';
?>

