<?php
include_once '../../koneksi/database.php';
isLogin();
isAdmin();

$id	= $_GET['id'];
$query = $mysql->execute("select * from user where `idUser`='".$mysql->clean($id)."' limit 1");

if ($query->num_rows>0){
	$edit = $query->fetch_array();
}else {
	header("Location: ".URL_WEB."aplikasi/daftar_user/?err=Id tidak ditemukan");
}

$judul = "Edit Akun";
include_once '../../template/Admin/header.php';
?>
<div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-user"></i> Edit User</h3> 
        </div>
        <div class="panel-body">
          <form action="edit_aksi.php" method="post">
          <input type="hidden" name="id" value="<?php echo $edit['idUser'];?>">
          <div class="">
            <p><label>Username</label><input type="text" class="form-control" name="nama" value="<?php echo $edit['user'];?>" /></p>
            <p><label>Password</label><input type="text" class="form-control" name="pass" value="<?php echo $edit['pass'];?>" /></p>
            <p><label>Level</label>
              <select class="form-control" id="id" name="level">
                <option <?php echo ($edit['level']==3 ? 'selected="selected"' : '');?> value="3">User</option>
                <option <?php echo ($edit['level']==2 ? 'selected="selected"' : '');?> value="2">Operator</option>
                <option <?php echo ($edit['level']=1 ? 'selected="selected"' : '');?> value="1">Admin</option>
              </select>
            </p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="btn btn-primary" type="submit" name="name" value="Edit" /></p>
          </div>
          </form>
        </div>
    </div>
<?php
include_once '../../template/Admin/footer.php';
?>

