<?php
include_once '../../koneksi/database.php';
isLogin();
isAdmin();

$judul = "Daftar User";
include_once '../../template/Admin/header.php';
?>
    <div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-user"></i> Daftar User <a href="tambah.php"><button class="btn btn-primary">Tambah</button></a></h3> 
        </div>
        <div class="panel-body">
          <table id="lookup_user" class="table table-bordered table-hover">  
             <thead bgcolor="#eeeeee" align="center">
              <tr>

              <th>ID</th>
               <th>Nama </th>
               <th>Level </th>
               <th class="text-center"> Action </th> 

              </tr>
             </thead>
              <tbody>
              </tbody>
          </table>
        </div>
    </div>
<?php
include_once '../../template/Admin/footer.php';
?>

