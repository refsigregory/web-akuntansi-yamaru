
<!DOCTYPE HTML>
<html>

<head>
  <title><?php echo $judul;?></title>
  <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
  <meta name="description" content="Yayasan yang memberikan pelayanan anak cacat, bantuan pada keluarga miskin, dan menyediakan lembaga pendidikan fisioterapi." />
  <meta name="keywords" content="yamaru, yayasan, anak cacat, panti asuhan, fisioterapi" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link type="text/css" href="<?php echo URL_WEB;?>assets/bootstrap/css/bootstrap.css" rel="stylesheet" />
  <link type="text/css" href="<?php echo URL_WEB;?>assets/bootstrap/css/bootstrap-theme.css" rel="stylesheet" />
  <link type="text/css" href="<?php echo URL_WEB;?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
  <style type="text/css">
      #jurnal_detail {
          display: none;
      }
  </style>
</head>

<body>
<div class="container">
  <div class="row">
    <div class="col-md-12">
          <nav class="navbar navbar-default">
            <div class="container-fluid">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo URL_WEB;?>aplikasi"><?php echo JUDUL;?></a>
              </div>

              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <?php if($user['level']==1) { ?>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Data Master <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo URL_WEB;?>aplikasi/daftar_akun">Daftar Akun</a></li>
                        <li><a href="<?php echo URL_WEB;?>aplikasi/daftar_user">Daftar User</a></li>
                        <li><a href="<?php echo URL_WEB;?>aplikasi/berita">Daftar Berita</a></li>
                      </ul>
                  </li>
                  <?php } ?>

                  <?php if(($user['level']==1) || ($user['level']==2)) { ?>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Data Transaksi <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                       <li><a href="<?php echo URL_WEB;?>aplikasi/entry_jurnal">Entry Jurnal</a></li>
                        <li><a href="<?php echo URL_WEB;?>aplikasi/list_jurnal">List Jurnal</a></li>
                        <li><a href="<?php echo URL_WEB;?>aplikasi/buku_besar">Buku Besar</a></li>
                        <!--<li><a href="#">Tutup Periode Aktif</a></li>-->
                      </ul>
                  </li>
                  <?php } ?>

                  <?php if(($user['level']==1) ||($user['level']==2) || ($user['level']==3)) { ?>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Laporan<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo URL_WEB;?>aplikasi/laporan/posisi_keuangan">Posisi Keuangan</a></li>
                        <li><a href="<?php echo URL_WEB;?>aplikasi/laporan/aktifitas">Aktivitas</a></li>
                      </ul>
                  </li>
                  <?php } ?>

                  </ul>
                  </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                  <li><a>Welcome, <b><?php echo $user['user'];?></b></a></li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tools <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="<?php echo URL_WEB;?>aplikasi/ganti_password">Ganti Password</a></li>
                      <?php if($user['level']==1) { ?>
                      <li><a href="#">Reset Aplikasi</a></li>
                      <?php } ?>
                      <li role="separator" class="divider"></li>
                      <li><a href="<?php echo URL_WEB;?>aplikasi/keluar.php">Keluar</a></li>
                    </ul>
                  </li>
                </ul>
              </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
          </nav>
<?php
if(isset($getmsg) || isset($_GET['msg'])){
$getmsg = $_GET['msg'];
}
if(isset($getmsg)){
?>
<div class="alert alert-success"><?php echo $getmsg;?></div>
<?php
}
?>
<?php
if(isset($geterror) || isset($_GET['error'])){
$geterror = $_GET['error'];
}
if(isset($geterror)){
?>
<div class="alert alert-danger"><?php echo $geterror;?></div>
<?php
}
?>