           <footer>
              <p>Copyright &copy; CV Credere | 2017</p>
            </footer>
          <p>&nbsp;</p>
          </div>
        </div>
    </div>
</div>
  <!-- javascript at the bottom for fast page loading -->
  <script src="<?php echo URL_WEB; ?>assets/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
  <script src="<?php echo URL_WEB; ?>assets/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
  <script src="<?php echo URL_WEB; ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        
  <script src="<?php echo URL_WEB; ?>assets/datatables/jquery.dataTables.js"></script>
  <script src="<?php echo URL_WEB; ?>assets/datatables/dataTables.bootstrap.js"></script>
  <script type="text/javascript" src="<?php echo URL_WEB; ?>assets/js/jquery-ui.js"></script>
  <script type="text/javascript" src="<?php echo URL_WEB; ?>assets/js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="<?php echo URL_WEB; ?>assets/js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="<?php echo URL_WEB; ?>assets/js/jquery.kwicks-1.5.1.js"></script>
  <script type="text/javascript" src="<?php echo URL_WEB; ?>assets/js/script.js"></script>
   <script src="<?php echo URL_WEB; ?>assets/js/jquery.form.js"></script> 

<script type="text/javascript">
  
// AJAX Image
$(document).ready(function() { 
         $('#upload_gambar').change(function() {
           $("#upload_gambar_berita").ajaxForm(function(data){

             tambah(data);
           }).submit();       
         });
       }); 
       function tambah(image){
         $("#preview").prepend(image);
         $('#notip').html('Menjalankan perintah:<br>move_uploaded_file($tmp_name, "img/$name");');
         $('.hapus').click(function(){             
           var gambar = $(this).attr('src'),
             gambarid = $(this).attr('id');
           $.post("<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>/uploads/gambar_berita.php", { hapus: gambar }).done(function(response) {
             $('#'+gambarid).remove();
             $('#notip').html(response);
           });
           return false;
         });
         $('#save').click(function(){
           $('#notip').html('Menjalankan perintah:<br>');
           $('.hapus').each(function(index, value) {
             $('#notip').append('Insert '+$(this).attr('src')+'<br>');
           });   
           return false;
         });
       }

</script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#images').kwicks({
        max : 600,
        spacing : 2
      });
      $('ul.sf-menu').sooperfish();
    });
  </script>
 <script type="text/javascript">
// hapus item

    $(".hapus").click(function () {
      alert("test");
        var jawab = confirm("Press a button!");
        if (jawab === true) {
//            kita set hapus false untuk mencegah duplicate request
            var hapus = false;
            if (!hapus) {
                hapus = true;
                $.post('hapus.php', {id: $(this).attr('data-id')},
                function (data) {
                    alert(data);
                });
                hapus = false;
            }
        } else {
            return false;
        }
    });
</script>
 
</body>
</html>