<!DOCTYPE php>
<php>

<head>
  <title><?php echo $judul;?></title>
  <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
  <meta name="description" content="Yayasan yang memberikan pelayanan anak cacat, bantuan pada keluarga miskin, dan menyediakan lembaga pendidikan fisioterapi." />
  <meta name="keywords" content="yamaru, yayasan, anak cacat, panti asuhan, fisioterapi" />
  <meta http-equiv="content-type" content="text/php; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="<?php echo URL_WEB;?>assets/css/style.css" />

  
  <!-- modernizr enables php5 elements and feature detects -->
  <script type="text/javascript" src="<?php echo URL_WEB;?>assets/js/modernizr-1.5.min.js"></script>
 
</head>

<body>
  <div id="main">
    <header>
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="<?php echo URL_WEB;?>index.php">YAYASAN<span class="logo_colour"> MANUEL RUNTU</span></a></h1>
          <h2>Mengentaskan Kemiskinan dan Meningkatkan Kualitas Hidup Masyarakat</h2>
        </div>
      </div>
      <nav>
        <ul class="sf-menu" id="nav">
          <li <?php $url = "/index.php"; echo ($_SERVER['PHP_SELF']==$url) ? 'class="selected"' : ''; ?>><a href="<?php echo URL_WEB;?>index.php">Home</a></li>
          <li <?php $url = "/news.php"; echo ($_SERVER['PHP_SELF']==$url) ? 'class="selected"' : ''; ?>><a href="<?php echo URL_WEB;?>news.php">News</a></li>
          <li <?php $url = "/about.php"; echo ($_SERVER['PHP_SELF']==$url) ? 'class="selected"' : ''; ?>><a href="<?php echo URL_WEB;?>about.php">About Us</a></li>
          <!--<li><a href="page.php">A Page</a></li>
          <li><a href="another_page.php">Another Page</a></li>
          <li><a href="#">Example Drop Down</a>
            <ul>
              <li><a href="#">Drop Down One</a></li>
              <li><a href="#">Drop Down Two</a>
                <ul>
                  <li><a href="#">Sub Drop Down One</a></li>
                  <li><a href="#">Sub Drop Down Two</a></li>
                  <li><a href="#">Sub Drop Down Three</a></li>
                  <li><a href="#">Sub Drop Down Four</a></li>
                  <li><a href="#">Sub Drop Down Five</a></li>
                </ul>
              </li>
              <li><a href="#">Drop Down Three</a></li>
              <li><a href="#">Drop Down Four</a></li>
              <li><a href="#">Drop Down Five</a></li>
            </ul>
          <li><a href="contact.php">Contact Us</a></li>
          </li>-->
          <li<?php $url = "/login.php"; echo ($_SERVER['PHP_SELF']==$url) ? ' class="selected"' : ''; ?>><a href="<?php echo URL_WEB;?>login.php">Login</a></li>
        </ul>
      </nav>
    </header>