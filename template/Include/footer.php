<footer>
      <p>Copyright &copy; CV Credere | 2017</p>
    </footer>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="<?php echo URL_WEB;?>assets/js/jquery.js"></script>
  <script type="text/javascript" src="<?php echo URL_WEB;?>assets/js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="<?php echo URL_WEB;?>assets/js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="<?php echo URL_WEB;?>assets/js/jquery.kwicks-1.5.1.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#images').kwicks({
        max : 600,
        spacing : 2
      });
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
