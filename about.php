<?php
  require_once 'koneksi/database.php';

  $judul = "Tentang Kami | ".JUDUL;
  require_once 'template/Include/header.php';
?>

    <div id="site_content">
      <ul id="images">
        <li><img src="images/1.jpg" width="600" height="300" alt="kegiatan yamaru 1" /></li>
        <li><img src="images/2.jpg" width="600" height="300" alt="kegiatan yamaru 2" /></li>
        <li><img src="images/3.jpg" width="600" height="300" alt="kegiatan yamaru 3" /></li>
        <li><img src="images/4.jpg" width="600" height="300" alt="kegiatan yamaru 4" /></li>
        <li><img src="images/5.jpg" width="600" height="300" alt="kegiatan yamaru 5" /></li>
        <li><img src="images/6.jpg" width="600" height="300" alt="kegiatan yamaru 6" /></li>
      </ul>
      <div id="sidebar_container">
        <div class="sidebar">
          <h3>Latest News</h3>
          <?php
            $query = $mysql->execute("select * from `berita` WHERE `status`='aktif' order by `tanggal` desc limit 5");
            
            while ($berita = $query->fetch_array())
            {
          ?>
          <h4><?php echo $berita['judul'];?></h4>
          <h5><?php echo $berita['tanggal'];?></h5>
          <p><?php echo limit_words($berita['berita'],10);?>...<a href="<?php echo URL_WEB;?>news.php?berita=<?php echo $berita['idBerita'];?>">Baca Selengkapnya</a></p>
          <?php
            }
          ?>
        </div>
      </div>
      
      <div class="content">
        <h1>About Us</h1>
        <h2>STRUKTUR ORGANISASI YAMARU</h2>

        <h3>Pelindung</h3>
        <h5>Uskup Manado : Mrg. Drs. J. Suwatan MSC.</h5>
        <h5>Br. Han Gerritse CSD</h5>

        <h3>Dewan Pembina</h3> 
        <h5>Imam-Direktur BTD : RP.Drs.Albertus Sujoko, MSC.</h5>
        <h5>Br. Markus Goran BTD.</h5>
        <h5>Br. Gregorius Palit BTD.</h5>

        <h3>Dewan Pengawas</h3>
        <h5>Br. Arnoldus BTD.</h5>
        <h5>Sdr. Erick Sumakud SE/MA.</h5>
        <h5>Sdr. Jerry Langi Amd. Ft.</h5>

        <h3>Dewan Pengurus</h3>
        <h5>Br. Marianus Elias Manuk BTD.</h5>
        <h5>Br. Berchmans Ngala BTD.</h5>
        <h5>Bpk. Oktafianus Ngala.</h5>

        <h2 id="h2lower">ALAMAT</h2>
        <h5>Woloan II / 323 Tomohon Barat 95422</h5>
        <h5>Kotak Pos 135 – Kota Tomohon – Sulawesi Utara - Indonesia</h5>
        <h5>Tel. 0431 - 352208</h5>

        <h2 id="h2lower">E-MAIL</h2>
        <h5>yamaru_ind@ymail.com</h5>

      </div>
    </div>
<?php
  require_once 'template/Include/footer.php';
?>
