-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 27 Feb 2017 pada 13.19
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `idBerita` int(11) NOT NULL,
  `judul` varchar(30) NOT NULL,
  `berita` text NOT NULL,
  `tanggal` date NOT NULL,
  `idUser` int(11) NOT NULL,
  `status` set('aktif','nonaktif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`idBerita`, `judul`, `berita`, `tanggal`, `idUser`, `status`) VALUES
(1, 'Berita Pertama', 'Testing berita pertama', '2017-01-11', 1, 'aktif'),
(3, 'Berita tidak aktif', 'test berita tidak aktif', '2017-01-23', 0, 'nonaktif'),
(6, 'Website Dalam Pengembangan', 'Untuk saat ini website ini masih dalam tahap pengembangan, mohon maaf jika ada fituer yang belum berfungsi.', '2017-01-23', 0, 'aktif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mutasi_akun`
--

CREATE TABLE `mutasi_akun` (
  `kodeakun` varchar(7) NOT NULL,
  `tanggal` date NOT NULL,
  `saldo_awal_debet` decimal(20,2) NOT NULL,
  `saldo_awal_kredit` decimal(20,2) NOT NULL,
  `mutasi_debet` decimal(20,2) NOT NULL,
  `mutasi_kredit` decimal(20,2) NOT NULL,
  `saldo_akhir_debet` decimal(20,2) NOT NULL,
  `saldo_akhir_kredit` decimal(20,2) NOT NULL,
  `saldo_awal` decimal(20,2) NOT NULL,
  `perubahan` decimal(20,2) NOT NULL,
  `saldo_akhir` decimal(20,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mutasi_akun`
--

INSERT INTO `mutasi_akun` (`kodeakun`, `tanggal`, `saldo_awal_debet`, `saldo_awal_kredit`, `mutasi_debet`, `mutasi_kredit`, `saldo_akhir_debet`, `saldo_akhir_kredit`, `saldo_awal`, `perubahan`, `saldo_akhir`) VALUES
('1-01-01', '2017-02-27', '0.00', '0.00', '3000000.00', '1000000.00', '2000000.00', '0.00', '0.00', '2000000.00', '2000000.00'),
('1-01-02', '2017-02-27', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
('1-01-03', '2017-02-27', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
('3-01-01', '2017-02-27', '0.00', '0.00', '1000000.00', '3000000.00', '0.00', '2000000.00', '0.00', '2000000.00', '2000000.00'),
('3-01-02', '2017-02-27', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mutasi_akun_detail`
--

CREATE TABLE `mutasi_akun_detail` (
  `no_index` bigint(20) UNSIGNED NOT NULL,
  `kodeakun` varchar(7) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `debet` decimal(20,2) NOT NULL,
  `kredit` decimal(20,2) NOT NULL,
  `saldo_debet` decimal(20,2) NOT NULL,
  `saldo_kredit` decimal(20,2) NOT NULL,
  `saldo` decimal(20,2) NOT NULL,
  `no_jurnal` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mutasi_akun_detail`
--

INSERT INTO `mutasi_akun_detail` (`no_index`, `kodeakun`, `tanggal`, `keterangan`, `debet`, `kredit`, `saldo_debet`, `saldo_kredit`, `saldo`, `no_jurnal`) VALUES
(1, '3-01-01', '2017-02-27', 'Pinjam uang', '0.00', '1000000.00', '0.00', '1000000.00', '1000000.00', '1'),
(2, '1-01-01', '2017-02-27', 'Pemasukan dari bank', '1000000.00', '0.00', '1000000.00', '0.00', '1000000.00', '1'),
(3, '3-01-01', '2017-02-27', '', '0.00', '1000000.00', '0.00', '2000000.00', '2000000.00', '2'),
(4, '1-01-01', '2017-02-27', 'pemasukan dari bank', '1000000.00', '0.00', '2000000.00', '0.00', '2000000.00', '2'),
(5, '3-01-01', '2017-02-27', '', '0.00', '1000000.00', '0.00', '3000000.00', '3000000.00', '3'),
(6, '1-01-01', '2017-02-27', '', '1000000.00', '0.00', '3000000.00', '0.00', '3000000.00', '3'),
(7, '1-01-01', '2017-02-27', '', '0.00', '1000000.00', '2000000.00', '0.00', '2000000.00', '4'),
(8, '3-01-01', '2017-02-27', '', '1000000.00', '0.00', '0.00', '2000000.00', '2000000.00', '4');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbakun`
--

CREATE TABLE `tbakun` (
  `kodeakun` varchar(7) NOT NULL,
  `namaak` varchar(60) NOT NULL,
  `saldonorm` set('DEBET','KREDIT') NOT NULL,
  `kelompok` set('AKTIVA','PASIVA') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbakun`
--

INSERT INTO `tbakun` (`kodeakun`, `namaak`, `saldonorm`, `kelompok`) VALUES
('1', 'Aktiva Lancar', 'DEBET', 'AKTIVA'),
('1-0', 'Kas dan Bank', 'DEBET', 'AKTIVA'),
('1-01', 'Kas', 'DEBET', 'AKTIVA'),
('1-01-01', 'Kas Yayasan', 'DEBET', 'AKTIVA'),
('1-01-02', 'Kas Kecil Yayasan', 'DEBET', 'AKTIVA'),
('1-01-03', 'Kas Kecil Akademi Fisioterapi', 'DEBET', 'AKTIVA'),
('1-01-04', 'Kas Kecil Sayap Kasih', 'DEBET', 'AKTIVA'),
('1-01-05', 'Kas Kecil A', 'DEBET', 'AKTIVA'),
('1-01-06', 'Kas Kecil B', 'DEBET', 'AKTIVA'),
('1-02', 'Bank Tidak Terikat', 'DEBET', 'AKTIVA'),
('1-02-01', 'Tabungan No. Rek. A', 'DEBET', 'AKTIVA'),
('1-02-02', 'Tabungan No. Rek. B', 'DEBET', 'AKTIVA'),
('1-02-10', 'Giro No. A', 'DEBET', 'AKTIVA'),
('1-02-11', 'Giro No. B', 'DEBET', 'AKTIVA'),
('1-03', 'Bank Terikat Sementara', 'DEBET', 'AKTIVA'),
('1-03-01', 'Tabungan No. Rek. C', 'DEBET', 'AKTIVA'),
('1-03-02', 'Tabungan No. Rek. D', 'DEBET', 'AKTIVA'),
('1-03-10', 'Giro No. C', 'DEBET', 'AKTIVA'),
('1-03-11', 'Giro No. D', 'DEBET', 'AKTIVA'),
('1-04', 'Bank Dana Pembangunan', 'DEBET', 'AKTIVA'),
('1-04-01', 'Tabungan Pembangunan No. A', 'DEBET', 'AKTIVA'),
('1-04-02', 'Tabungan Pembangunan No. B', 'DEBET', 'AKTIVA'),
('1-04-10', 'Giro Pembangunan No.', 'DEBET', 'AKTIVA'),
('1-1', 'Deposito', 'DEBET', 'AKTIVA'),
('1-10', 'Deposito Tidak Terikat', 'DEBET', 'AKTIVA'),
('1-10-01', 'Deposito A', 'DEBET', 'AKTIVA'),
('1-10-02', 'Deposito B', 'DEBET', 'AKTIVA'),
('1-11', 'Deposito Terikat Sementara', 'DEBET', 'AKTIVA'),
('1-11-01', 'Deposito C', 'DEBET', 'AKTIVA'),
('1-11-02', 'Deposito D', 'DEBET', 'AKTIVA'),
('1-12', 'Deposito Dana Pembangunan', 'DEBET', 'AKTIVA'),
('1-12-01', 'Deposito E', 'DEBET', 'AKTIVA'),
('1-12-02', 'Deposito F', 'DEBET', 'AKTIVA'),
('1-2', 'Piutang', 'DEBET', 'AKTIVA'),
('1-20', 'Piutang Karyawan', 'DEBET', 'AKTIVA'),
('1-20-01', 'Piutang a.n. A', 'DEBET', 'AKTIVA'),
('1-20-02', 'Piutang a.n. B', 'DEBET', 'AKTIVA'),
('1-21', 'Piutang Lain-lain', 'DEBET', 'AKTIVA'),
('1-21-01', 'Piutang A', 'DEBET', 'AKTIVA'),
('1-21-02', 'Piutang B', 'DEBET', 'AKTIVA'),
('1-3', 'Biaya Dibayar di Muka dan Uang Muka', 'DEBET', 'AKTIVA'),
('1-30', 'Biaya Dibayar di Muka', 'DEBET', 'AKTIVA'),
('1-30-01', 'Biaya Dibayar di Muka A', 'DEBET', 'AKTIVA'),
('1-30-02', 'Biaya Dibayar di Muka B', 'DEBET', 'AKTIVA'),
('1-31', 'Uang Muka', 'DEBET', 'AKTIVA'),
('1-31-01', 'Uang Muka A', 'DEBET', 'AKTIVA'),
('1-31-02', 'Uang Muka B', 'DEBET', 'AKTIVA'),
('1-4', 'Pajak Dibayar di Muka', 'DEBET', 'AKTIVA'),
('1-40', 'Pajak Penghasilan', 'DEBET', 'AKTIVA'),
('1-40-01', 'Pajak Penghasilan Pasal 25', 'DEBET', 'AKTIVA'),
('1-40-02', 'Pajak Penghasilan Pasal 21', 'DEBET', 'AKTIVA'),
('1-5', 'Persediaan', 'DEBET', 'AKTIVA'),
('1-50', 'Persediaan Barang', 'DEBET', 'AKTIVA'),
('1-50-01', 'Persediaan Buku', 'DEBET', 'AKTIVA'),
('1-50-02', 'Persediaan Kalender', 'DEBET', 'AKTIVA'),
('1-50-03', 'Persediaan Kaset', 'DEBET', 'AKTIVA'),
('1-50-04', 'Persediaan Patung', 'DEBET', 'AKTIVA'),
('1-50-05', 'Persediaan Benda Rohani Lainnya', 'DEBET', 'AKTIVA'),
('1-6', 'Perlengkapan', 'DEBET', 'AKTIVA'),
('1-60', 'Perlengkapan Kantor', 'DEBET', 'AKTIVA'),
('1-60-01', 'Perlengkapan Kantor Yayasan', 'DEBET', 'AKTIVA'),
('1-60-02', 'Perlengkapan Kantor Akademi Fisioterapi', 'DEBET', 'AKTIVA'),
('1-60-03', 'Perlengkapan Kantor Sayap Kasih', 'DEBET', 'AKTIVA'),
('2', 'Utang', 'KREDIT', 'PASIVA'),
('2-1', 'Aktiva Tetap', 'DEBET', 'AKTIVA'),
('2-10-01', 'Tanah Yayasan Lokasi', 'DEBET', 'AKTIVA'),
('2-10-02', 'Tanah Akademi Fisioterapi Lokasi', 'DEBET', 'AKTIVA'),
('2-10-03', 'Tanah Sayap Kasih Lokasi', 'DEBET', 'AKTIVA'),
('2-10-11', 'Gedung Yayasan Lokasi', 'DEBET', 'AKTIVA'),
('2-10-12', 'Gedung Akademi Fisioterapi Lokasi', 'DEBET', 'AKTIVA'),
('2-10-13', 'Gedung Sayap Kasih Lokasi', 'DEBET', 'AKTIVA'),
('2-10-21', 'Peralatan Yayasan', 'DEBET', 'AKTIVA'),
('2-10-22', 'Peralatan Akademi Fisioterapi', 'DEBET', 'AKTIVA'),
('2-10-23', 'Peralatan Sayap Kasih', 'DEBET', 'AKTIVA'),
('2-10-31', 'Prasarana Yayasan', 'DEBET', 'AKTIVA'),
('2-10-32', 'Prasarana Akademi Fisioterapi', 'DEBET', 'AKTIVA'),
('2-10-33', 'Prasarana Sayap Kasih', 'DEBET', 'AKTIVA'),
('2-10-41', 'Kendaraan Yayasan', 'DEBET', 'AKTIVA'),
('2-10-42', 'Kendaraan Akademi Fisioterapi', 'DEBET', 'AKTIVA'),
('2-10-43', 'Kendaraan Sayap Kasih', 'DEBET', 'AKTIVA'),
('2-19', 'Akumulasi Penyusutan', 'KREDIT', 'AKTIVA'),
('2-19-01', 'Akumulasi Penyusutan Gedung Yayasan', 'KREDIT', 'AKTIVA'),
('2-19-02', 'Akumulasi Penyusutan Gedung Akademi Fisioterapi', 'KREDIT', 'AKTIVA'),
('2-19-03', 'Akumulasi Penyusutan Gedung Sayap Kasih', 'KREDIT', 'AKTIVA'),
('2-19-21', 'Akumulasi Penyusutan Peralatan Yayasan', 'KREDIT', 'AKTIVA'),
('2-19-22', 'Akumulasi Penyusutan Peralatan Akademi Fisioterapi', 'KREDIT', 'AKTIVA'),
('2-19-23', 'Akumulasi Penyusutan Peralatan Sayap Kasih', 'KREDIT', 'AKTIVA'),
('2-19-31', 'Akumulasi Penyusutan Prasarana Yayasan', 'KREDIT', 'AKTIVA'),
('2-19-32', 'Akumulasi Penyusutan Prasarana Akademi Fisioterapi', 'KREDIT', 'AKTIVA'),
('2-19-33', 'Akumulasi Penyusutan Sayap Kasih', 'KREDIT', 'AKTIVA'),
('2-19-41', 'Akumulasi Penyusutan Kendaraan Yayasan', 'KREDIT', 'AKTIVA'),
('2-19-42', 'Akumulasi Penyusutan Kendaraan Akademi Fisioterapi', 'KREDIT', 'AKTIVA'),
('2-19-43', 'Akumulasi Penyusutan Kendaraan Sayap Kasih', 'KREDIT', 'AKTIVA'),
('2-5', 'Aktiva Lain = lain', 'DEBET', 'AKTIVA'),
('2-50-01', 'Aktiva Lain - lain Yayasan.', 'DEBET', 'AKTIVA'),
('2-50-02', 'Aktiva Lain - lain Akademi Fisioterapi', 'DEBET', 'AKTIVA'),
('2-50-03', 'Aktiva Lain - lain Sayap Kasih', 'DEBET', 'AKTIVA'),
('2-59', 'Gedung dan Prasarana Dalam Proses', 'DEBET', 'AKTIVA'),
('2-59-01', 'Gedung Yayasan Dalam Proses', 'DEBET', 'AKTIVA'),
('2-59-02', 'Gedung Akademi Fisioterapi Dalam Proses ', 'DEBET', 'AKTIVA'),
('2-59-03', 'Gedung Sayap Kasih Dalam Proses', 'DEBET', 'AKTIVA'),
('2-59-10', 'Prasarana Yayasan Dalam Proses', 'DEBET', 'AKTIVA'),
('2-59-11', 'Prasarana Akademi Fisioterapi Dalam Proses', 'DEBET', 'AKTIVA'),
('2-59-12', 'Prasarana Sayap Kasih Dalam Proses', 'DEBET', 'AKTIVA'),
('3', 'Kewajiban dan Aktiva Bersih', 'KREDIT', 'PASIVA'),
('3-0', 'Kewajiban Jangka Pendek', 'KREDIT', 'PASIVA'),
('3-01', 'Hutang Bank', 'KREDIT', 'PASIVA'),
('3-01-01', 'Hutang Bank A', 'KREDIT', 'PASIVA'),
('3-01-02', 'Hutang Bank B', 'KREDIT', 'PASIVA'),
('3-01-11', 'Bagian Lancar Hutang Jangka Panjang', 'KREDIT', 'PASIVA'),
('3-04', 'Hutang Pengadaan Aktiva Tetap', 'KREDIT', 'PASIVA'),
('3-04-01', 'Hutang Pengadaan Aktiva Tetap - Kas', 'KREDIT', 'PASIVA'),
('3-05', 'Hutang Lain - lain', 'KREDIT', 'PASIVA'),
('3-05-01', 'Hutang Lain - Kas', 'KREDIT', 'PASIVA'),
('3-06', 'Biaya dan Pajak Yang Masih Harus Dibayar', 'KREDIT', 'PASIVA'),
('3-06-01', 'Biaya - biaya Yang Masih Harus Dibayar', 'KREDIT', 'PASIVA'),
('3-06-11', 'Pajak Penghasilan Pasal 25 Yang Masih Harus Dibayar', 'KREDIT', 'PASIVA'),
('3-06-12', 'Pajak Penghasilan Pasal 21 Yang Masih Harus Dibayar', 'KREDIT', 'PASIVA'),
('3-2', 'Kewajiban Jangka Panjang', 'KREDIT', 'PASIVA'),
('3-20', 'Hutang Jangka Panjang', 'KREDIT', 'PASIVA'),
('3-20-01', 'Hutang Jangka Panjang Bank', 'KREDIT', 'PASIVA'),
('3-20-11', 'Hutang Jangka Panjang Pada Pihak Lain', 'KREDIT', 'PASIVA'),
('3-20-21', 'Hutang Jangka Panjang Lain - lain', 'KREDIT', 'PASIVA'),
('3-5', 'Aktiva Bersih', 'KREDIT', 'PASIVA'),
('3-50', 'Aktiva Bersih Tidak Terikat', 'KREDIT', 'PASIVA'),
('3-50-01', 'Aktiva Bersih Tidak Terikat s/d Periode Lalu', 'KREDIT', 'PASIVA'),
('3-51', 'Aktiva Bersih Terikat Sementara', 'KREDIT', 'PASIVA'),
('3-51-01', 'Aktiva Bersih Terikat Sementara s/d Periode Lalu', 'KREDIT', 'PASIVA'),
('3-52', 'Aktiva Bersih Pembangunan', 'KREDIT', 'PASIVA'),
('3-52-1', 'Aktiva Bersih Pembangunan Periode Berjalan', 'KREDIT', 'PASIVA'),
('3-52-11', 'Sumbangan Pembangunan Pemerintah', 'KREDIT', 'PASIVA'),
('3-52-12', 'Sumbangan Pembangunan Keuskupan', 'KREDIT', 'PASIVA'),
('3-52-13', 'Sumbangan Pembangunan Umat', 'KREDIT', 'PASIVA'),
('3-52-14', 'Sumbangan Pembangunan Masyarakat', 'KREDIT', 'PASIVA'),
('3-52-15', 'Sumbangan Pembangunan Donatur tetap', 'KREDIT', 'PASIVA'),
('3-52-16', 'Sumbangan Pembangunan Lainnya', 'KREDIT', 'PASIVA'),
('3-52=01', 'Aktiva Bersih Pembangunan s/d Periode Lalu', 'KREDIT', 'PASIVA'),
('3-53', 'Aktiva Bersih Lain - lain', 'KREDIT', 'PASIVA'),
('3-53-0', 'Aktiva Bersih Lain - lain s/d Periode Lalu', 'KREDIT', 'PASIVA'),
('3-53-01', 'Aktiva Bersih Lain - lain s/d Periode Lalu', 'KREDIT', 'PASIVA'),
('3-54', 'Kenaikan/Penurunan Aktiva Bersih', 'KREDIT', 'PASIVA'),
('3-54-0', 'Aktiva Bersih Lain - lain Periode Berjalan', 'KREDIT', 'PASIVA'),
('3-54-01', 'Aktiva Bersih Lain - lain Periode Berjalan', 'KREDIT', 'PASIVA'),
('4 ', 'Penerimaan', 'KREDIT', 'PASIVA'),
('4-0', 'Penerimaan Aktiva Bersih Tidak Terikat', 'KREDIT', 'PASIVA'),
('4-01-01', 'Penerimaan Aktiva Bersih Tidak Terikat Sumbangan ...', 'KREDIT', 'PASIVA'),
('4-01-02', 'Penerimaan Aktiva Bersih Tidak Terikat...', 'KREDIT', 'PASIVA'),
('4-02', 'Penerimaan Lain - lain', 'KREDIT', 'PASIVA'),
('4-02-01', 'Penerimaan Lain - lain dari...', 'KREDIT', 'PASIVA'),
('4-1', 'Penerimaan Aktiva Bersih Terikat Sementara', 'KREDIT', 'PASIVA'),
('4-10-01', 'Penerimaan Aktiva Bersih Terikat Sementara ...', 'KREDIT', 'PASIVA'),
('4-10-02', 'Penerimaan Aktiva Bersih Terikat Sementara ...', 'KREDIT', 'PASIVA'),
('5', 'Biaya/Beban/Pengeluaran', 'DEBET', 'AKTIVA'),
('5-0', 'Biaya Aktiva Bersih Tidak Terikat', 'DEBET', 'AKTIVA'),
('5-01', 'Biaya ABTT Yayasan', 'DEBET', 'AKTIVA'),
('5-01-01', 'Biaya ABTT Yayasan...', 'DEBET', 'AKTIVA'),
('5-01-02', 'Biaya ABTT Yayasan...', 'DEBET', 'AKTIVA'),
('5-02', 'Biaya ABTT Akademi Fisioterapi', 'DEBET', 'AKTIVA'),
('5-02-01', 'Biaya ABTT Akademi Fisioterapi ...', 'DEBET', 'AKTIVA'),
('5-02-02', 'Biaya ABTT Akademi Fisioterapi ...', 'DEBET', 'AKTIVA'),
('5-03', 'Biaya ABTT Sayap Kasih', 'DEBET', 'AKTIVA'),
('5-03-01', 'Biaya ABTT Sayap Kasih ...', 'DEBET', 'AKTIVA'),
('5-03-02', 'Biaya ABTT Sayap Kasih ...', 'DEBET', 'AKTIVA');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbjurnal`
--

CREATE TABLE `tbjurnal` (
  `no_jurnal` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `total_debit` decimal(22,2) NOT NULL,
  `total_kredit` decimal(22,2) NOT NULL,
  `entry_user` varchar(20) NOT NULL,
  `waktu_entry` date NOT NULL,
  `disetujui_oleh` varchar(50) NOT NULL,
  `terima_bayar` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbjurnal`
--

INSERT INTO `tbjurnal` (`no_jurnal`, `tanggal`, `keterangan`, `total_debit`, `total_kredit`, `entry_user`, `waktu_entry`, `disetujui_oleh`, `terima_bayar`) VALUES
('1', '2017-02-27', 'Meminjam uang dari bank', '1000000.00', '1000000.00', '1', '2017-02-27', 'admin', 'test'),
('2', '2017-02-27', 'abe doi di bank', '1000000.00', '1000000.00', '1', '2017-02-27', 'admin', 'test'),
('3', '2017-02-27', 'ambe lagi', '1000000.00', '1000000.00', '1', '2017-02-27', 'admin', 'tes'),
('4', '2017-02-27', 'bayar utang', '1000000.00', '1000000.00', '1', '2017-02-27', 'admin', 'sdafsdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbjurnaldetil`
--

CREATE TABLE `tbjurnaldetil` (
  `no_id` int(11) NOT NULL,
  `no_jurnal` varchar(10) NOT NULL,
  `kodeakun` varchar(7) NOT NULL,
  `no_urut` int(11) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `jumlah_debet` decimal(22,2) NOT NULL,
  `jumlah_kredit` decimal(22,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbjurnaldetil`
--

INSERT INTO `tbjurnaldetil` (`no_id`, `no_jurnal`, `kodeakun`, `no_urut`, `keterangan`, `jumlah_debet`, `jumlah_kredit`) VALUES
(1, '1', '3-01-01', 1, 'Pinjam uang', '0.00', '1000000.00'),
(2, '1', '1-01-01', 2, 'Pemasukan dari bank', '1000000.00', '0.00'),
(3, '2', '3-01-01', 1, '', '0.00', '1000000.00'),
(4, '2', '1-01-01', 2, 'pemasukan dari bank', '1000000.00', '0.00'),
(5, '3', '3-01-01', 1, '', '0.00', '1000000.00'),
(6, '3', '1-01-01', 2, '', '1000000.00', '0.00'),
(7, '4', '1-01-01', 1, '', '0.00', '1000000.00'),
(8, '4', '3-01-01', 2, '', '1000000.00', '0.00'),
(9, '5', '1-20-01', 1, '', '1000000.00', '0.00'),
(10, '5', '1-20-01', 2, '', '0.00', '1000000.00'),
(11, '8', '1-20-01', 1, 'pittang a', '1000000.00', '0.00'),
(12, '8', '1-02-01', 2, '', '0.00', '1000000.00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbjurnaldetiltemp`
--

CREATE TABLE `tbjurnaldetiltemp` (
  `no_id` int(11) NOT NULL,
  `kodeakun` varchar(7) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `jumlah_debet` decimal(22,2) NOT NULL,
  `jumlah_kredit` decimal(22,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbjurnaltemp`
--

CREATE TABLE `tbjurnaltemp` (
  `no_jurnal` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `total_debit` decimal(22,2) NOT NULL,
  `total_kredit` decimal(22,2) NOT NULL,
  `entry_user` varchar(20) NOT NULL,
  `waktu_entry` date NOT NULL,
  `disetujui_oleh` varchar(50) NOT NULL,
  `terima_bayar` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `idUser` int(11) NOT NULL,
  `user` varchar(20) NOT NULL,
  `pass` varchar(20) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`idUser`, `user`, `pass`, `level`) VALUES
(1, 'admin', 'admin', 1),
(2, 'operator', 'operator', 2),
(3, 'user', 'user', 3),
(4, 'admin2', 'admin2', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`idBerita`),
  ADD KEY `idUser` (`idUser`);

--
-- Indexes for table `mutasi_akun`
--
ALTER TABLE `mutasi_akun`
  ADD PRIMARY KEY (`kodeakun`);

--
-- Indexes for table `mutasi_akun_detail`
--
ALTER TABLE `mutasi_akun_detail`
  ADD PRIMARY KEY (`no_index`),
  ADD UNIQUE KEY `no_index` (`no_index`),
  ADD KEY `kodeakun` (`kodeakun`),
  ADD KEY `no_jurnal` (`no_jurnal`);

--
-- Indexes for table `tbakun`
--
ALTER TABLE `tbakun`
  ADD PRIMARY KEY (`kodeakun`);

--
-- Indexes for table `tbjurnal`
--
ALTER TABLE `tbjurnal`
  ADD PRIMARY KEY (`no_jurnal`);

--
-- Indexes for table `tbjurnaldetil`
--
ALTER TABLE `tbjurnaldetil`
  ADD PRIMARY KEY (`no_id`),
  ADD KEY `kodeakun` (`kodeakun`),
  ADD KEY `no_jurnal` (`no_jurnal`);

--
-- Indexes for table `tbjurnaldetiltemp`
--
ALTER TABLE `tbjurnaldetiltemp`
  ADD PRIMARY KEY (`no_id`),
  ADD KEY `kodeakun` (`kodeakun`);

--
-- Indexes for table `tbjurnaltemp`
--
ALTER TABLE `tbjurnaltemp`
  ADD PRIMARY KEY (`no_jurnal`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`idUser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `idBerita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `mutasi_akun_detail`
--
ALTER TABLE `mutasi_akun_detail`
  MODIFY `no_index` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbjurnaldetil`
--
ALTER TABLE `tbjurnaldetil`
  MODIFY `no_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tbjurnaldetiltemp`
--
ALTER TABLE `tbjurnaldetiltemp`
  MODIFY `no_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `mutasi_akun`
--
ALTER TABLE `mutasi_akun`
  ADD CONSTRAINT `mutasi_akun_ibfk_1` FOREIGN KEY (`kodeakun`) REFERENCES `tbakun` (`kodeakun`);

--
-- Ketidakleluasaan untuk tabel `mutasi_akun_detail`
--
ALTER TABLE `mutasi_akun_detail`
  ADD CONSTRAINT `mutasi_akun_detail_ibfk_2` FOREIGN KEY (`no_jurnal`) REFERENCES `tbjurnal` (`no_jurnal`),
  ADD CONSTRAINT `mutasi_akun_detail_ibfk_3` FOREIGN KEY (`kodeakun`) REFERENCES `mutasi_akun` (`kodeakun`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
