<?php
  require_once 'koneksi/database.php';

  $judul = JUDUL;
  require_once 'template/Include/header.php';
?>

    <div id="site_content">
      <ul id="images">
        <li><img src="images/1.jpg" width="600" height="300" alt="kegiatan yamaru 1" /></li>
        <li><img src="images/2.jpg" width="600" height="300" alt="kegiatan yamaru 2" /></li>
        <li><img src="images/3.jpg" width="600" height="300" alt="kegiatan yamaru 3" /></li>
        <li><img src="images/4.jpg" width="600" height="300" alt="kegiatan yamaru 4" /></li>
        <li><img src="images/5.jpg" width="600" height="300" alt="kegiatan yamaru 5" /></li>
        <li><img src="images/6.jpg" width="600" height="300" alt="kegiatan yamaru 6" /></li>
      </ul>
      <div id="sidebar_container">
        <div class="sidebar">
          <h3>Latest News</h3>
          <?php
            $query = $mysql->execute("select * from `berita` WHERE `status`='aktif' order by `tanggal` desc limit 5");
            
            while ($berita = $query->fetch_array())
            {
          ?>
          <h4><?php echo $berita['judul'];?></h4>
          <h5><?php echo $berita['tanggal'];?></h5>
          <p><?php echo limit_words($berita['berita'],10);?>...<a href="<?php echo URL_WEB;?>news.php?berita=<?php echo $berita['idBerita'];?>">Baca Selengkapnya</a></p>
          <?php
            }
          ?>
        </div>
      </div>
      <div class="content">
        <h1>Welcome to Yamaru Home Page</h1>
        <p><img src="images/logo.jpg" alt="logo yamaru" width="286" height="296" class="left">Yayasan Manuel Runtu (Yamaru) adalah yayasan yang memberikan pelayanan anak cacat, bantuan pada keluarga miskin, dan menyediakan lembaga pendidikan fisioterapi. Misi yang diemban adalah untuk mengentaskan kemiskinan dan meningkatkan kualitas hidup masyarakat.</p>
        <p>Didirikan pada tanggal 15 September 1985, Yamaru kini mengelola tiga unit kerja yakni Akademi Fisioterapi St. Lukas, Panti Asuhan Orang Cacat - Sayap Kasih, dan unit kerja pertanian, Toko Pax, KAPARE, serta Pasar Murah.</p>
        <p>Mari turut serta dalam misi memberikan pelayanan kepada anak-anak cacat, memberikan bantuan kepada keluarga-keluarga miskin, dan menyediakan lembaga pendidikan kesehatan fisioterapi.</p>
      </div>
    </div>
<?php
  require_once 'template/Include/footer.php';
?>